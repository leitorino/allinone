<?php

use Illuminate\Database\Seeder;
use AllInOne\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

    factory(AllInOne\Product::class, 20)->create();

    }
}
