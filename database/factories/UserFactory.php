<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(AllInOne\Product::class, function (Faker $faker) {
    return [
        'category_id' => 1,
        'product_name' => $faker->unique()->word,
        'product_code' => $faker->unique()->hexcolor,
        'description' => $faker->text($maxNbChars = 200),
        'precio' => $faker->randomDigitNotNull,
        'compras' => $faker->randomDigitNotNull,
        'image' => '786862.png',
    ];
});
