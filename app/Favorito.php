<?php

namespace AllInOne;

use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
	public function prod(){
    	return $this->belongsTo('AllInOne\Product','product_id');
    }
}
