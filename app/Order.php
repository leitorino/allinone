<?php

namespace AllInOne;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function orders(){
    	return $this->hasMany('AllInOne\OrdersDetails','order_id');
    }
}
