<?php

namespace AllInOne\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Image;
use AllInOne\Publicidad;
use Illuminate\Support\Facades\Input;

class PublicidadController extends Controller
{
        public function addPublicidad(Request $request){
    	if(Session::has('adminSession')){
    		if($request->isMethod('post')){
    			$data = $request->all();

    			$publicidad = new Publicidad;


				if(empty($data['status'])){
					$publicidad->status = 0;
				}else{
					$publicidad->status = 1;
				}

				//imagen
				if($request->hasFile('publicidad_image')){
					$img_tmp = Input::file('publicidad_image');
					if($img_tmp->isValid()){
						$extension = $img_tmp->getClientOriginalExtension();
						$filename = rand(111,999999).'.'.$extension;
						$big_image_path = 'images/frontend_images/publicidad/';
						$img_tmp->move($big_image_path,$filename);
						$publicidad->image = $filename;
					}
				}else{
					$publicidad->image = '';
				}
				$publicidad->save();
				return redirect('/admin/view-publicidad')->with('flash_message_success','Anuncio Publicitario Agregado Satisfactoriamente');
    		}
    		return view('admin.publicidad.add_publicidad');
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }

    public function editPublicidad(Request $request, $id = null){
    	if(Session::has('adminSession')){
    		if ($request->isMethod('post')) {
    			$data = $request->all();

    			if(empty($data['status'])){
    				$data['status'] = '0';
    			}else{
    				$data['status'] = '1';
    			}

    			if($request->hasFile('publicidad_image')){
					$img_tmp = Input::file('publicidad_image');
					if($img_tmp->isValid()){
						$extension = $img_tmp->getClientOriginalExtension();
						$filename = rand(111,999999).'.'.$extension;
						$big_image_path = 'images/frontend_images/publicidad/'.$filename;
						//tamaño de la imagen;
						$img = Image::make($img_tmp);
						$img_size = $img->filesize();
						if($img_size <13000000){
							$img->save($big_image_path);
						}else{
							return redirect('/admin/view-publicidad')->with('flash_message_error','La imagen excede el limite de tamaño');
						}	
					}
				}else{
					$filename = $data['current_image'];
				}

				Publicidad::where(['id'=>$id])->update(['image' => $filename,'status' => $data['status'],'image' => $filename]);

				return redirect('/admin/view-publicidad')->with('flash_message_success','Publicidad Editada satisfactoriamente');
    		}else{
    			$publicidadDetails = Publicidad::Where('id',$id)->first();
    			return view('admin.publicidad.edit_publicidad')->with(compact('publicidadDetails'));
    		}
			
    		
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }

    public function viewPublicidad(Request $request){
    	if(Session::has('adminSession')){
			$publicidades = Publicidad::get();
			$publicidades = json_decode(json_encode($publicidades));
    		return view('admin.publicidad.view_publicidad')->with(compact('publicidades'));
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }

    public function deletePublicidad(Request $request, $id = null){
    	if(Session::has('adminSession')){
			Publicidad::where(['id' => $id])->delete();
    		return redirect('/admin/view-publicidad')->with('flash_message_success','El Anuncio Publicitario Ha Sido Eliminado Exitosamente');
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }
}
