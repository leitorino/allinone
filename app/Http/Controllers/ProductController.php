<?php

namespace AllInOne\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use AllInOne\Category;
use AllInOne\Product;
use AllInOne\ProductsImage;
use AllInOne\OrdersDetails;
use AllInOne\Order;
use AllInOne\User;
use AllInOne\Ranking;
use AllInOne\Comment;
use AllInOne\Favorito;
use AllInOne\Publicidad;
use Illuminate\Support\Facades\Input;
use Image;
use DB;

class ProductController extends Controller
{
    public function addProduct(Request $request){
        if(Session::has('adminSession')){
            if($request->isMethod('post')){
                $data = $request->all();
                if(empty($data['category_id'])){
                    return redirect('/admin/add-product')->with('flash_message_error','Falta Seleccionar una Categoria');
                }
                $product = new Product;
                $product->category_id = $data['category_id'];
                $product->product_name = $data['product_name'];
                $product->product_code = $data['product_code'];

                if(!empty($data['desc'])){
                    $product->description = $data['desc'];
                }else{
                    $product->description = '';
                }

                if(!empty($data['descdet'])){
                    $product->descriptiondetallado = $data['descdet'];
                }else{
                    $product->descriptiondetallado = '';
                }

                $product->compras = 0;
                $product->precio = $data['precio'];

                //imagen
                if($request->hasFile('product_image')){
                    $img_tmp = Input::file('product_image');
                    if($img_tmp->isValid()){
                        $extension = $img_tmp->getClientOriginalExtension();
                        $filename = rand(111,999999).'.'.$extension;
                        $big_image_path = 'images/backend_images/products/big/'.$filename;
                        $medium_image_path = 'images/backend_images/products/medium/'.$filename;
                        $small_image_path = 'images/backend_images/products/small/'.$filename;
                        //tamaño de la imagen;
                        $img = Image::make($img_tmp);
                        $img_size = $img->filesize();
                        if($img_size <13000000){
                            $img->resize(800,800)->save($big_image_path);
                            $img->resize(400,400)->save($medium_image_path);
                            $img->resize(50,50)->save($small_image_path);
                            //guardar nombre en la bbdd
                            $product->image = $filename;
                        }else{
                            return redirect('/admin/add-product')->with('flash_message_error','La imagen excede el limite de tamaño');
                        }   
                    }
                }else{
                    $product->image = '';
                }


                $product->save();
                return redirect('/admin/view-product')->with('flash_message_success','Producto agregado satisfactoriamente');
            }
            $categories = Category::where(['parent_id' => 0])->get();
            $categories_dropdown = "";
            foreach ($categories as $cat) {
                $categories_dropdown .= "<option value='".$cat->id."'>".$cat->name."</option>";
                $sub_categories = Category::where(['parent_id' => $cat->id])->get();
                foreach ($sub_categories as $sub_cat) {
                    $categories_dropdown .= "<option value='".$sub_cat->id."'>&nbsp;--&nbsp;".$sub_cat->name."</option>";
                }
            }
            return view('admin.products.add_product')->with(compact('categories_dropdown'));
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function viewProduct(Request $request){
        if(Session::has('adminSession')){
            $products = Product::get();
            $products = json_decode(json_encode($products));
            foreach ($products as $key => $val) {
                /*$ids = Category::get();
                $ids = json_decode(json_encode($ids));
                die;
                foreach ($ids['0']->id as $id_category) {
                    if($id_category == $val->category_id){
                        $products[$key]->category_name = $ids['0']->name;
                    }else{
                        $products[$key]->category_name = "Sin Categoria";
                    }
                }*/
                $category_name = Category::where(['id'=>$val->category_id])->first();
                $products[$key]->category_name = $category_name->name;

            }
            //echo '<prev>'; print_r($categories); die;
            return view('admin.products.view_product')->with(compact('products'));
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function editProduct(Request $request, $id = null){
        if(Session::has('adminSession')){

            if($request->isMethod('post')){
                $data = $request->all();
                //imagen
                if($request->hasFile('product_image')){
                    $img_tmp = Input::file('product_image');
                    if($img_tmp->isValid()){
                        $extension = $img_tmp->getClientOriginalExtension();
                        $filename = rand(111,999999).'.'.$extension;
                        $big_image_path = 'images/backend_images/products/big/'.$filename;
                        $medium_image_path = 'images/backend_images/products/medium/'.$filename;
                        $small_image_path = 'images/backend_images/products/small/'.$filename;
                        //tamaño de la imagen;
                        $img = Image::make($img_tmp);
                        $img_size = $img->filesize();
                        if($img_size <13000000){
                            $img->resize(800,800)->save($big_image_path);
                            $img->resize(400,400)->save($medium_image_path);
                            $img->resize(50,50)->save($small_image_path);
                        }else{
                            return redirect('/admin/add-product')->with('flash_message_error','La imagen excede el limite de tamaño');
                        }   
                    }
                }else{
                    $filename = $data['current_image'];
                }
                //dd($filename);
                if(empty($data['desc'])){
                    $data['desc'] = '';
                }


                if(empty($data['descdet'])){
                    $data['descdet'] = '';
                }

                Product::where(['id'=>$id])->update(['product_name' => $data['product_name'],'product_code' => $data['product_code'],'description' => $data['desc'], 'descriptiondetallado' => $data['descdet'], 'precio' => $data['precio'],'image' => $filename]);

                return redirect('/admin/view-product')->with('flash_message_success','Producto editado satisfactoriamente');
            }

            $productDetails = Product::where(['id' => $id])->first();
            $categories = Category::where(['parent_id' => 0])->get();
            $categories_dropdown = "";
            foreach ($categories as $cat) {
                if($cat->$id==$productDetails->category_id){
                    $selected = "selected";
                }else{
                    $selected = "";
                }
                $categories_dropdown .= "<option value='".$cat->id." ".$selected."'>".$cat->name."</option>";
                $sub_categories = Category::where(['parent_id' => $cat->id])->get();
                foreach ($sub_categories as $sub_cat) {
                    if($sub_cat->$id==$productDetails->category_id){
                        $selected = "selected";
                    }else{
                        $selected = "";
                    }
                    $categories_dropdown .= "<option value='".$sub_cat->id." ".$selected."'>&nbsp;--&nbsp;".$sub_cat->name."</option>";
                }
            }
            return view('admin.products.edit_product')->with(compact('productDetails','categories_dropdown'));
            
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function deleteProductImage($id = null){
        if(Session::has('adminSession')){
            $productImage = Product::where(['id'=>$id])->first();

            $big_image_path = 'images/backend_images/products/big/';
            $medium_image_path = 'images/backend_images/products/medium/';
            $small_image_path = 'images/backend_images/products/small/';

            if(file_exists($big_image_path.$productImage->image)){
                unlink($big_image_path.$productImage->image);
            }

            if(file_exists($medium_image_path.$productImage->image)){
                unlink($medium_image_path.$productImage->image);
            }

            if(file_exists($small_image_path.$productImage->image)){
                unlink($small_image_path.$productImage->image);
            }

            Product::where(['id'=>$id])->update(['image'=>'']);

            return redirect()->with('flash_message_success','Imagen del producto borrada');
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function deleteProduct($id = null){
        if(Session::has('adminSession')){
            if(!empty($id)){
                Product::where(['id'=>$id])->delete();
                return redirect('/admin/view-product')->with('flash_message_success','Producto Eliminado satisfactoriamente');
            }
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function addAttributes(Request $request, $id = null){
        if(Session::has('adminSession')){
            if($request->isMethod('post')){
                
                
            }
            $productDetails = Product::where(['id'=>$id])->first();
            return view('admin.products.add_attribute')->with(compact('productDetails'));
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function addImages(Request $request, $id = null){

        if(Session::has('adminSession')){
            if($request->isMethod('post')){
                
                $data = $request->all();

                if($request->hasFile('image')){
                    $files = Input::file('image');
                    foreach ($files as $file) {
                        $image = new ProductsImage;
                        $extension = $file->getClientOriginalExtension();
                        $filename = rand(111,999999).'.'.$extension;
                        $big_image_path = 'images/backend_images/products/big/'.$filename;
                        $medium_image_path = 'images/backend_images/products/medium/'.$filename;
                        $small_image_path = 'images/backend_images/products/small/'.$filename;
                        //tamaño de la imagen;
                        $img = Image::make($file);
                        $img_size = $img->filesize();
                        if($img_size <13000000){
                            $img->resize(800,800)->save($big_image_path);
                            $img->resize(400,400)->save($medium_image_path);
                            $img->resize(50,50)->save($small_image_path);
                            //guardar nombre en la bbdd
                            $image->image = $filename;
                            $image->product_id = $id;
                            $image->save();
                            return redirect('/admin/view-product')->with('flash_message_error','Imagenes Subidas Satisfactoriamente');
                    }else{
                        return redirect('/admin/view-product')->with('flash_message_error','La imagen excede el limite de tamaño');
                        }
                    }
                }

            }
            
            $productDetails = Product::where(['id'=>$id])->first();
            $productDetails = json_decode(json_encode($productDetails));
            $images = ProductsImage::where(['product_id' => $id])->get();
            $images = json_decode(json_encode($images));
            return view('admin.products.add_images')->with(compact('productDetails','images'));
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }

    }

    public function products($url = null){
        //mostrar 404
        $countCategory = Category::where(['url' => $url])->count();
        if($countCategory == 0){
            abort(404);
        }


        $categoryDetails = Category::where(['url' => $url])->first();
        $categoryDetails = json_decode(json_encode($categoryDetails ));
        $cant_prod = Product::where(['category_id' => $categoryDetails->id])->orderBy('compras','desc')->get();
        $cant_prod = json_decode(json_encode($cant_prod));
        $cant_prod = count($cant_prod);
        $products = Product::where(['category_id' => $categoryDetails->id])->orderBy('compras','desc')->paginate(9);
        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories_dropdown = "";
        $scategories = "";
        $sub_categories = Category::where(['parent_id' => $categoryDetails->id])->get();
        foreach ($sub_categories as $sub_cat) {
            $scategories .= $sub_cat->name;
        }
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }

        $sub_categories = json_decode(json_encode($sub_categories));

        $publicidad = Publicidad::orderBy(DB::raw('RAND()'))->first();
        $publicidad = json_decode(json_encode($publicidad));

        return view('products.category')->with(compact('categoryDetails','products','categories_dropdown','sub_categories','cant_prod','categories','url','publicidad'));
    }

    public function productsOrdenados($url = null,$sort = null){

        $categoryDetails = Category::where(['url' => $url])->first();
        $categoryDetails = json_decode(json_encode($categoryDetails ));

        if($sort == 'mayor'){
            $cant_prod = Product::where(['category_id' => $categoryDetails->id])->orderBy('precio','desc')->get();
            $cant_prod = json_decode(json_encode($cant_prod));
            $cant_prod = count($cant_prod);
            $products = Product::where(['category_id' => $categoryDetails->id])->orderBy('precio','desc')->paginate(9);
        }elseif($sort == 'menor'){
            $cant_prod = Product::where(['category_id' => $categoryDetails->id])->orderBy('precio','asc')->get();
            $cant_prod = json_decode(json_encode($cant_prod));
            $cant_prod = count($cant_prod);
            $products = Product::where(['category_id' => $categoryDetails->id])->orderBy('precio','asc')->paginate(9);
        }elseif ($sort == 'ultimas') {
            $cant_prod = Product::where(['category_id' => $categoryDetails->id])->orderBy('id','desc')->get();
            $cant_prod = json_decode(json_encode($cant_prod));
            $cant_prod = count($cant_prod);
            $products = Product::where(['category_id' => $categoryDetails->id])->orderBy('id','desc')->paginate(9);
        }elseif ($sort == 'dest'){
            $cant_prod = Product::where(['category_id' => $categoryDetails->id])->orderBy('compras','desc')->get();
            $cant_prod = json_decode(json_encode($cant_prod));
            $cant_prod = count($cant_prod);
            $products = Product::where(['category_id' => $categoryDetails->id])->orderBy('compras','desc')->paginate(9);
        }else{
            return redirect($url);
        }

        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories_dropdown = "";
        $scategories = "";
        $sub_categories = Category::where(['parent_id' => $categoryDetails->id])->get();
        foreach ($sub_categories as $sub_cat) {
            $scategories .= $sub_cat->name;
        }
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }
        return view('products.category')->with(compact('categoryDetails','products','categories_dropdown','sub_categories','cant_prod','url'));
    }

    public function allProducts(){

        $cant_prod = Product::orderBy('compras','desc')->get();
        $cant_prod = json_decode(json_encode($cant_prod));
        $cant_prod = count($cant_prod);
        $products = Product::orderBy('compras','desc')->paginate(9);
        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories = json_decode(json_encode($categories));
        $categories_dropdown = "";
        $sub_categories = [];
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }
        return view('products.category')->with(compact('products','categories_dropdown','sub_categories','cant_prod','categories'));
    }

    public function product($id = null){

        $productDetails = Product::where('id',$id)->first();

        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories_dropdown = "";

        $produc_promo = Product::where(['category_id' => 7])->first();
        $produc_promo = json_decode(json_encode($produc_promo));

        $images = ProductsImage::where(['product_id' => $id])->get();
        $images = json_decode(json_encode($images));

        $comments = Comment::with('names')->where(['product_id' => $id])->get();
        $comments = json_decode(json_encode($comments));

        $stars = Ranking::where(['product_id' => $id])->get();
        $stars = json_decode(json_encode($stars));

        $products_rel = Product::where(['category_id' => $productDetails->category_id])->orderBy('compras','desc')->get();
        $products_rel = json_decode(json_encode($products_rel));

        $starsprom=0;
        for ($i=0; $i <5 ; $i++) { 
            $starsind[$i] = 0;
        }
        $cont=0;

        foreach ($stars as $star) {

            if($star->puntos == 1){

                $starsind[0] = $starsind[0] + 1;

            }elseif($star->puntos == 2){

                $starsind[1] = $starsind[1] + 1;

            }elseif($star->puntos == 3){

                $starsind[2] = $starsind[2] + 1;

            }elseif($star->puntos == 4){

                $starsind[3] = $starsind[3] + 1;

            }elseif($star->puntos == 5){

                $starsind[4] = $starsind[4] + 1;

            }

            $starsprom = $star->puntos + $starsprom;
            $cont = $cont + 1;

        }

        if($cont > 0){
            $starsprom = intval($starsprom / $cont);
            $cont2 = $cont;
        }else{
            $starsprom = 0;
            $cont2 = 1;
        }

        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }
        return view('products.product')->with(compact('categories_dropdown','productDetails', 'images', 'starsprom', 'starsind', 'products_rel','cont','cont2','comments','id','produc_promo'));

    }

    public function addtocart(Request $request){
        $data = $request->all();

        if(!isset($data['user_email'])){
            if(Auth::check()){
                $data['user_email'] = Auth::user()->email;
            }else{
                $data['user_email'] = '';
            }
        }


        if(!Session::has('session_id')){
            $data['session_id'] = str_random(50);
            Session::put('session_id',$data['session_id']);
        }else{
            $data['session_id'] = Session::get('session_id');
        }


        if(!isset($data['plan'])){
            $data['plan'] = 1;
        }

        if (Auth::check()) {
            $usercart = DB::table('cart')
            ->where('user_id', Auth::user()->id)
            ->orWhere('session_id' , Session::get('session_id'))
            ->get();

            $usercart = json_decode(json_encode($usercart));

            if($usercart == []){
                DB::table('cart')->insert(['product_id'=>$data['product_id'],'product_name'=>$data['product_name'],'product_code'=>$data['product_code'],'precio'=>$data['precio'],'cantidad'=>$data['cantidad'],'plan'=>$data['plan'],'user_email'=>$data['user_email'],'session_id'=>$data['session_id'],'user_id'=>Auth::user()->id]);
            }else{
                foreach ($usercart as $item) {
                    if($item->product_id != $data['product_id']){
                        DB::table('cart')->insert(['product_id'=>$data['product_id'],'product_name'=>$data['product_name'],'product_code'=>$data['product_code'],'precio'=>$data['precio'],'cantidad'=>$data['cantidad'],'plan'=>$data['plan'],'user_email'=>$data['user_email'],'session_id'=>$data['session_id'],'user_id'=>Auth::user()->id]);
                    }else{
                        return redirect('carrito')->with('flash_message_error', 'El Producto Ya Se Encuentra En El Carrito');
                    }
                }
            }
            
        }else{

            $usercart = DB::table('cart')->where(['session_id' => Session::get('session_id')])->get();

            $usercart = json_decode(json_encode($usercart));

            if($usercart == []){
                DB::table('cart')->insert(['product_id'=>$data['product_id'],'product_name'=>$data['product_name'],'product_code'=>$data['product_code'],'precio'=>$data['precio'],'cantidad'=>$data['cantidad'],'plan'=>$data['plan'],'user_email'=>$data['user_email'],'session_id'=>$data['session_id']]);
            }else{
                foreach ($usercart as $item) {
                    if($item->product_id != $data['product_id']){
                        DB::table('cart')->insert(['product_id'=>$data['product_id'],'product_name'=>$data['product_name'],'product_code'=>$data['product_code'],'precio'=>$data['precio'],'cantidad'=>$data['cantidad'],'plan'=>$data['plan'],'user_email'=>$data['user_email'],'session_id'=>$data['session_id']]);
                    }else{
                        return redirect('carrito')->with('flash_message_error', 'El Producto Ya Se Encuentra En El Carrito');
                    }
                }
            }   
        }   

        return redirect('carrito')->with('flash_message_success','El Producto Ha Sido Agregado Al Carrito');
    }

    public function cart(Request $request){

        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories_dropdown = "";

        $produc_promo = Product::where(['category_id' => 7])->first();
        $produc_promo = json_decode(json_encode($produc_promo));

        if(Auth::check()){
            $usercart = DB::table('cart')
            ->where('user_id', Auth::user()->id)
            ->orWhere('session_id' , Session::get('session_id'))
            ->get();
        }else{
            $usercart = DB::table('cart')->where(['session_id' => Session::get('session_id')])->get();
        }
        $usercart = json_decode(json_encode($usercart));


        $products = array();
        $cont=0;

        foreach ($usercart as $item) {
            $product = Product::where('id',$item->product_id)->first();
            $product = json_decode(json_encode($product));
            if ($cont>0) {
                foreach ($products as $element) {
                    if ($element->id != $product->id) {
                        array_push($products,$product);
                    }
                }
            }else{
                array_push($products,$product);
            }
            
            $cont++;
        }
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }

        return view('products.cart')->with(compact('categories_dropdown','products','usercart','produc_promo'));

    }

    public function deleteCartProduct($id = null){
        DB::table('cart')->where('id',$id)->delete();
        return redirect('carrito')->with('flash_message_success','El Producto Ha Sido Borrado Del Carrito');
    }

    public function ajaxRequestPost(Request $request){
        $data = $request->all();
        if(isset($data['cant'])){
            DB::table('cart')->where('id', $data['id'])->update(['cantidad' => $data['cant']]);
        }

        if(isset($data['plan'])){
            DB::table('cart')->where('id', $data['id'])->update(['plan' => $data['plan']]);
        }

        if(Auth::check()){
            DB::table('cart')->where('id', $data['id'])->update(['user_id' => Auth::user()->id]);
        }

    }

    public function sendComment(Request $request){
        $data = $request->all();
        if(isset($data)){
            $comment = new Comment;
            $comment->user_id = $data['user_id'];
            $comment->product_id = $data['product_id'];
            $comment->comment = $data['comentario'];
            $comment->save();
        }

    }

    public function pedido(Request $request){

        $order = new Order;
        $order->user_id = Auth::user()->id;
        $order->email = Auth::user()->email;
        $total = 0;

        $order->status = 'Esperando Pago';
        $order->total = 0;

        $cart_products = DB::table('cart')->where(['user_id'=>Auth::user()->id])->get();
        $cart_products = json_decode(json_encode($cart_products));

        if(($cart_products == [])){
            return redirect('/carrito')->with('flash_message_error','No Se Ha Podido Realizar La Compra');;
        }

        $order->save();
        $order_id = DB::getPdo()->lastInsertId();
        Session::put('order',$order_id);
        
        foreach ($cart_products as $prod) {
            $cartProd = new OrdersDetails;
            $cartProd->order_id = $order_id;
            $cartProd->user_id = Auth::user()->id;
            $cartProd->product_id = $prod->product_id;
            $oldprod = Product::where('id',$prod->product_id)->get();
            $oldprod = json_decode(json_encode($oldprod));
            $newshop = $oldprod[0]->compras + 1;
            Product::where('id',$prod->product_id)->update(['compras' => $newshop]);
            $cartProd->plan = $prod->plan;
            $cartProd->cantidad = $prod->cantidad;
            $total = $total + (($prod->precio * $prod->plan) * $prod->cantidad);
            $cartProd->save();
        }
        
        $order->total = $total;
        DB::table('orders')->where('id', $order_id)->update(['total' => $total]);

        
        $json = json_encode($order_id);
        Session::put('total',$total);
        header('Content-Type: application/json');
        echo $json;

    }

    public function paypal(Request $request){
        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories_dropdown = "";
        return view('products.paypal')->with(compact('categories_dropdown'));
    }

    public function thanksPaypal(Request $request){
        DB::table('cart')->where('user_id',Auth::user()->id)->delete();
        DB::table('orders')->where('id', Session::get('order'))->update(['status' => 'Nuevo']);
        Session::forget('total');
        Session::forget('order_id'); 
        return redirect('/pedidos')->with('flash_message_success','Gracias Por Su Compra');
    }

    public function cancelPaypal(){
        Order::where(['id'=>Session::get('order')])->delete();
        OrdersDetails::where(['order_id'=>Session::get('order')])->delete();
        Session::forget('total');
        Session::forget('order'); 
        return redirect('/carrito')->with('flash_message_error','No Se Ha Podido Realizar La Compra');;
    }

    public function crm(){
        // $order = DB::table('orders')->where('id', )->get();
        // $order = json_decode(json_encode($order));
        // $order = $order[0];
        $user = DB::table('users')->where('id',Auth::user()->id)->get();
        $user = json_decode(json_encode($user));
        $user = $user[0];
        $orderDetails = Order::with('orders')->where('id',Session::get('order'))->first();
        $orderDetails = json_decode(json_encode($orderDetails));
        $products = Product::get();
        $products = json_decode(json_encode($products));
        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories_dropdown = "";
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }
        //dd($order,$user);
        return view('products.crm')->with(compact('orderDetails','user','categories_dropdown','products'));
    }

    public function userOrders(){
        $user_id = Auth::user()->id;
        $orders = Order::with('orders')->where('user_id',$user_id)->orderBy('id','DESC')->paginate(5);
        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories_dropdown = "";

        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }
        return view('products.user_orders')->with(compact('orders','categories_dropdown'));
    }

    public function userOrderDetails($order_id){
        $user_id = Auth::user()->id;
        $orderDetails = Order::with('orders')->where('id',$order_id)->first();
        $orderDetails = json_decode(json_encode($orderDetails));
        $products = Product::get();
        $products = json_decode(json_encode($products));
        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories_dropdown = "";
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }
        return view('products.user_orders_details')->with(compact('orderDetails','products','categories_dropdown'));
    }

    public function viewOrders(){
        if(Session::has('adminSession')){
            $orders = Order::with('orders')->orderBy('id','Desc')->get();
            $orders = json_decode(json_encode($orders));
            return view('admin.orders.view_orders')->with(compact('orders'));
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder'); 
        }
    }

    public function deleteOrders(){
        if (Session::has('adminSession')) {
            Order::where(['status'=>'Esperando Pago'])->delete();
            return redirect('/admin/view-orders')->with('flash_message_success','Ordenes Eliminadas Satisfactoriamente');
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder'); 
        }
    }

    public function viewOrderDetails($order_id){
        if(Session::has('adminSession')){
            $orderDetails = Order::with('orders')->where('id',$order_id)->first();
            $orderDetails = json_decode(json_encode($orderDetails));
            $user_id = $orderDetails->user_id;
            $userDetails = User::where('id',$user_id)->first();
            $products = Product::get();
            $products = json_decode(json_encode($products));
            return view('admin.orders.order_details')->with(compact('orderDetails','userDetails','products'));
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function updateOrderStatus(Request $request){
        if(Session::has('adminSession')){
            if($request->isMethod('post')){
                $data = $request->all();
                Order::where('id',$data['order_id'])->update(['status'=>$data['order_status']]);
                return redirect()->back()->with('flash_message_success','Estado de la Orden Actualizada!');
            }
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function searchProducts(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/


        $search_product = $data['product'];

        $products = Product::where('product_name','like','%'.$search_product.'%')->paginate(9);
        $cant_prod = count($products);
        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories = json_decode(json_encode($categories));
        $categories_dropdown = "";
        $sub_categories = [];
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }   

            return view('products.category')->with(compact('categories','products','search_product','sub_categories','cant_prod','categories_dropdown')); 

        }
    }

    public function favoritos(Request $request){

        $products = Favorito::with('prod')->where(['user_id' => Auth::user()->id])->get();
        $products = json_decode(json_encode($products));

        $categories = Category::where(['parent_id' => 0,'status' => 1])->get();
        $categories_dropdown = "";
        foreach ($categories as $cat) {
            $categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
        }

        return view('products.favoritos')->with(compact('categories_dropdown','products'));

    }

    public function deleteFavorito($id = null){
        Favorito::where(['id'=>$id])->delete();
        return redirect('/favoritos')->with('flash_message_success','El Producto Ha Sido Borrado De Favoritos'); 
    }

    public function addFavorito(Request $request){
        $data = $request->all();
        if(isset($data)){
            $favoritos = Favorito::where([['user_id','=',Auth::user()->id],['product_id','=',$data['product_id']]])->get();
            $favoritos = json_decode(json_encode($favoritos));
            if($favoritos == []){
                $favorito = new Favorito;
                $favorito->user_id = Auth::user()->id;
                $favorito->product_id = $data['product_id'];
                $favorito->save();
            }
        
        }

    }

    public function calificarProd(Request $request){
        $data = $request->all();
        if (isset($data)) {
            $calificaciones = Ranking::where([['user_id','=',Auth::user()->id],['product_id','=',$data['product_id']]])->get();
            $calificaciones = json_decode(json_encode($calificaciones));
            if ($calificaciones == []) {
                $calificacion = new Ranking;
                $calificacion->user_id = Auth::user()->id;
                $calificacion->product_id = $data['product_id'];
                $calificacion->puntos = $data['calificacion'];
                $calificacion->save();
            }else{
                Ranking::where([['user_id','=',Auth::user()->id],['product_id','=',$data['product_id']]])->update(['puntos'=>$data['calificacion']]);
            }
        }
    }

    public function viewComment(){
        if(Session::has('adminSession')){
            $comments = Comment::get();
            $comments = json_decode(json_encode($comments));
            return view('admin.comments.view_comments')->with(compact('comments'));
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function deleteComment($id = null){
        Comment::where(['id'=>$id])->delete();
        return redirect('/view-comments')->with('flash_message_success','El Comentario Ha Sido Eliminado'); 
    }

}

