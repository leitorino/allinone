<?php

namespace AllInOne\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Image;
use AllInOne\Banner;
use Illuminate\Support\Facades\Input;

class BannersController extends Controller
{
    public function addBanner(Request $request){
    	if(Session::has('adminSession')){
    		if($request->isMethod('post')){
    			$data = $request->all();

    			$banner = new Banner;
				$banner->title = $data['banner_title'];
				$banner->link = $data['banner_link'];

				if(empty($data['status'])){
					$banner->status = 0;
				}else{
					$banner->status = 1;
				}

				//imagen
				if($request->hasFile('banner_image')){
					$img_tmp = Input::file('banner_image');
					if($img_tmp->isValid()){
						$extension = $img_tmp->getClientOriginalExtension();
						$filename = rand(111,999999).'.'.$extension;
						$big_image_path = 'images/frontend_images/banners/'.$filename;
						$img = Image::make($img_tmp);
						$img_size = $img->filesize();
						if($img_size <13000000){
							$img->save($big_image_path);
							//guardar nombre en la bbdd
							$banner->image = $filename;
						}else{
							return redirect('/admin/add-banner')->with('flash_message_error','La imagen excede el limite de tamaño');
						}	
					}
				}else{
					$banner->image = '';
				}
				$banner->save();
				return redirect('/admin/view-banners')->with('flash_message_success','Banner Agregado satisfactoriamente');
    		}
    		return view('admin.banners.add_banner');
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }

    public function editBanner(Request $request, $id = null){
    	if(Session::has('adminSession')){
    		if ($request->isMethod('post')) {
    			$data = $request->all();

    			if(empty($data['status'])){
    				$data['status'] = '0';
    			}else{
    				$data['status'] = '1';
    			}

    			if (empty($data['banner_title'])) {
    				$data['banner_title'] = '';
    			}

    			if (empty($data['banner_link'])) {
    				$data['banner_link'] = '';
    			}

    			if($request->hasFile('banner_image')){
					$img_tmp = Input::file('banner_image');
					if($img_tmp->isValid()){
						$extension = $img_tmp->getClientOriginalExtension();
						$filename = rand(111,999999).'.'.$extension;
						$big_image_path = 'images/frontend_images/banners/'.$filename;
						//tamaño de la imagen;
						$img = Image::make($img_tmp);
						$img_size = $img->filesize();
						if($img_size <13000000){
							$img->save($big_image_path);
						}else{
							return redirect('/admin/view-banners')->with('flash_message_error','La imagen excede el limite de tamaño');
						}	
					}
				}else{
					$filename = $data['current_image'];
				}

				Banner::where(['id'=>$id])->update(['image' => $filename,'title' => $data['banner_title'],'link' => $data['banner_link'],'status' => $data['status'],'image' => $filename]);

				return redirect('/admin/view-banners')->with('flash_message_success','Banner Editado satisfactoriamente');
    		}else{
    			$bannerDetails = Banner::Where('id',$id)->first();
    			return view('admin.banners.edit_banner')->with(compact('bannerDetails'));
    		}
			
    		
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }

    public function viewBanners(Request $request){
    	if(Session::has('adminSession')){
			$banners = Banner::get();
			$banners = json_decode(json_encode($banners));
    		return view('admin.banners.view_banner')->with(compact('banners'));
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }

    public function deleteBanner(Request $request, $id = null){
    	if(Session::has('adminSession')){
			Banner::where(['id' => $id])->delete();
    		return redirect('/admin/view-banners')->with('flash_message_success','El Banner Ha Sido Eliminado Exitosamente');
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }
}