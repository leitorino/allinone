<?php

namespace AllInOne\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Session;
use AllInOne\User;
use AllInOne\Order;
use AllInOne\Comment;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function login(Request $request){
    	if($request->IsMethod('post')){
    		$data = $request->input();
    		if(Auth::attempt(['username'=>$data['username'],'password'=>$data['password'],'admin'=>'1'])){
    			//echo "Completado"; die;
    			Session::put('adminSession',$data['username']);
    			return redirect('admin/dashboard');
    		}else{
    			//echo "fallo"; die;
    			return redirect('/admin')->with('flash_message_error', 'Usuario o Contraseña invalida');
    		}		
    	}else{
            if(Session::has('adminSession')){
                return redirect('admin/dashboard');
            }else{
                return view('admin.admin_login');
            }
        }
    	
    }

    public function dashboard(){
    	if(Session::has('adminSession')){
            $date_act = getdate()['year'].'-'.getdate()['mon'].'-'.getdate()['mday'].' '.getdate()['hours'].':'.getdate()['minutes'].':'.getdate()['seconds'];
            $users_cont = User::count();
            $newusers_cont = User::where('created_at',$date_act)->count();
            $orders_cont = Order::count();
            $orderspend_cont = Order::where('status','Nuevo')->count();
            $comments = Comment::with('names')->orderBy('id','asc')->limit(3)->get();
            $comments = json_decode(json_encode($comments));
    		return view('admin.dashboard')->with(compact('users_cont','orders_cont','comments','newusers_cont','orderspend_cont'));
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }

    public function settings(){
        if(Session::has('adminSession')){
            return view('admin.settings');
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function chkPassword(Request $request){
        if(Session::has('adminSession')){
            $data = $request -> all();
            $current_password = $data["current_pwd"];
            $check_password = User::where(['username' => Auth::user()->username])->first();
            if(Hash::check($current_password,$check_password->password)){
                echo "true"; die;
            }else{
                echo "false"; die;
            }
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function updatePassword(Request $request){
        if(Session::has('adminSession')){
            if($request->IsMethod('post')){
            	$data = $request->all();
            	//echo'<prev>';print_r($data); die;
            	$check_password = User::where(['username' => Auth::user()->username])->first();
            	echo($check_password);
            	$current_password = $data['current_pwd'];
            	if(Hash::check($current_password,$check_password->password)){
            		$password = bcrypt($data['new_pwd']);
            		User::where('id',Auth::user()->id)->update(['password'=>$password]);
            		return redirect('/admin/settings')->with('flash_message_success','Contraseña Actualizada Exitosamente');
            	}else{
            		return redirect('/admin/settings')->with('flash_message_error','Contraseña No Actualizada');
            	}

            }
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function logout(){
    	Session::flush();
    	return redirect('/admin')->with('flash_message_success', 'Ha cerrado sesión satisfactoriamente');
    }


}
