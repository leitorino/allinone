<?php

namespace AllInOne\Http\Controllers;

use Illuminate\Http\Request;
use AllInOne\Banner;
use AllInOne\Category;
use AllInOne\Product;

class FrontController extends Controller
{
    public function index(Request $request){
		$products = Product::orderBy('compras','desc')->get();
		$products = json_decode(json_encode($products));
		$categories = Category::where(['status' => 1])->get();
		$categories = json_decode(json_encode($categories));
		$toptenproducts = array();
		$toptenproducts2 = array();
		$categoryname = array();
		$categoryurl = array();
		$cont = 0;
		foreach ($products as $product) {
			if($cont < 10){
				array_push($toptenproducts,$product);
				$cont++;
			}
		}
		
		$cont = 0;
		foreach ($categories as $category) {
			$cont2 = 0;
			$cant = 0;
			foreach ($products as $product) {
				if($category->id == $product->category_id){
					if ($cont2 == 0) {
						$cant = $product->compras;
					}else{
						$cant = $cant + $product->compras;
					}
					$cont2++;
				}		
			}
			if($cont == 0){
				$contprodcat = array("$category->id"=>"$cant");
			}else{
				$contprodcat = $contprodcat + (array("$category->id"=>"$cant"));
			}
			$cont++;
		}
		if(isset($contprodcat)){
			arsort($contprodcat);

			$cont = 0;

			foreach ($contprodcat as $key => $value) {
				$cont2 = 0;
				$toptenproducts2[$cont] = array();
				if($cont < 3){

					$products = Product::where(['category_id'=>$key])->orderBy('compras', 'DESC')->get();
					$products = json_decode(json_encode($products));

					foreach ($categories as $category) {
						if($category->id == $key){
							$categoryname[$cont]=$category->name;
							$categoryurl[$cont]=$category->url;
						}
					}
					
					foreach ($products as $product) {
						if($cont2 < 10){		
							array_push($toptenproducts2[$cont],$product);
							$cont2++;
						}
					}

				}
				$cont++;
			}
		}
		
		

		$categories = Category::where(['parent_id' => 0,'status' => 1])->get();
		$categories_dropdown = "";
		foreach ($categories as $cat) {
			$categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
		}

		$banners = Banner::get();
		$banners = json_decode(json_encode($banners));
		return view('welcome')->with(compact('categoryname','toptenproducts','toptenproducts2','contprodcat','categories','categories_dropdown', 'categoryurl','banners'));
	}

	public function categorias(Request $request){

		$categories = Category::where(['parent_id' => 0,'status' => 1])->get();
		$categories = json_decode(json_encode($categories));
		$subcategories = Category::where('parent_id','<>','0')->get();
		$subcategories = json_decode(json_encode($subcategories));
		$categories_dropdown = "";
		foreach ($categories as $cat) {
			$categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
		}

		return view('categorias')->with(compact('categories','subcategories','categories_dropdown'));
	}

	public function ayuda(){

		$categories = Category::where(['parent_id' => 0,'status' => 1])->get();
		$categories = json_decode(json_encode($categories));
		$subcategories = Category::where('parent_id','<>','0')->get();
		$subcategories = json_decode(json_encode($subcategories));
		$categories_dropdown = "";
		foreach ($categories as $cat) {
			$categories_dropdown .= "<option value='".$cat->name."'>".$cat->name."</option>";
		}

		return view('ayuda')->with(compact('categories','subcategories','categories_dropdown'));
	}


}
