<?php

namespace AllInOne\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Session;
use AllInOne\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function login(Request $request){
    	if($request->IsMethod('post')){
    		$data = $request->input();
    		if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password']])){
    			//echo "Completado"; die;
    			Session::put('userSession',$data['email']);
    			return redirect('/');
    		}else{
    			//echo "fallo"; die;
    			return redirect('login')->with('flash_message_error', 'Usuario o Contraseña invalida');
    		}		
    	}
    }

    public function viewUsers(){
        if(Session::has('adminSession')){
            $users = User::where('id','<>','0')->get();
            $users = json_decode(json_encode($users));
            return view('admin.users.view_users')->with(compact('users'));
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }

    public function editUser(Request $request, $id = null){
        if(Session::has('adminSession')){
            if($request->isMethod('post')){

                $data = $request->all();

                if(empty($data['status'])){
                    $useradmin = 0;
                }else{
                    $useradmin = 1;
                }

                User::where(['id'=>$id])->update(['name'=>$data['user_name'],'admin'=>$useradmin]);
                return redirect('/admin/view-user')->with('flash_message_success','Usuario Editado satisfactoriamente');
            }
            $userdetail = User::where(['id'=>$id])->first();
            return view('admin.users.edit_users')->with(compact('userdetail'));
        }else{
            return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
        }
    }
}
