<?php

namespace AllInOne\Http\Controllers;

use Illuminate\Http\Request;
use AllInOne\Category;
use Session;
use Illuminate\Support\Facades\Input;
use Image;


class CategoryController extends Controller
{
    
	public function addCategory(Request $request){
		if(Session::has('adminSession')){
    		if($request->isMethod('post')){
				$data = $request->all();
				$category = new Category;
				$category->name = $data['category_name'];
				$category->parent_id = $data['parent_id'];
				$category->description = $data['desc'];
				$category->url = $data['url'];

				if(empty($data['status'])){
					$category->status = 0;
				}else{
					$category->status = 1;
				}


				if($request->hasFile('category_image')){
					$img_tmp = Input::file('category_image');
					if($img_tmp->isValid()){
						$extension = $img_tmp->getClientOriginalExtension();
						$filename = rand(111,999999).'.'.$extension;
						$big_image_path = 'images/backend_images/category/big/'.$filename;
						$small_image_path = 'images/backend_images/category/small/'.$filename;
						//tamaño de la imagen;
						$img = Image::make($img_tmp);
						$img_size = $img->filesize();
						if($img_size <13000000){
							$img->save($big_image_path);
							$img->resize(50,50)->save($small_image_path);
							//guardar nombre en la bbdd
							$category->image = $filename;
						}else{
							return redirect('/admin/add-category')->with('flash_message_error','La imagen excede el limite de tamaño');
						}	
					}
				}else{
					$category->image = '';
				}

				$category->save();
				return redirect('/admin/view-category')->with('flash_message_success','Categoria agregada satisfactoriamente');
			}
			$levels = Category::where(['parent_id' => 0])->get();
			return view('admin.categories.add_category')->with(compact('levels'));
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
    }
		

	public function viewCategory(Request $request){
		if(Session::has('adminSession')){
			$categories = Category::get();
			$categories = json_decode(json_encode($categories));
			//echo '<prev>'; print_r($categories); die;
    		return view('admin.categories.view_category')->with(compact('categories'));
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
	}

	public function editCategory(Request $request, $id = null){
		if(Session::has('adminSession')){
			if($request->isMethod('post')){
				$data = $request->all();

				if(empty($data['status'])){
					$categorystatus = 0;
				}else{
					$categorystatus = 1;
				}

				if($request->hasFile('category_image')){
					$img_tmp = Input::file('category_image');
					if($img_tmp->isValid()){
						$extension = $img_tmp->getClientOriginalExtension();
						$filename = rand(111,999999).'.'.$extension;
						$big_image_path = 'images/backend_images/category/big/'.$filename;
						$small_image_path = 'images/backend_images/category/small/'.$filename;
						//tamaño de la imagen;
						$img = Image::make($img_tmp);
						$img_size = $img->filesize();
						if($img_size <13000000){
							$img->resize(800,800)->save($big_image_path);
							$img->resize(50,50)->save($small_image_path);
						}else{
							return redirect('/admin/add-category')->with('flash_message_error','La imagen excede el limite de tamaño');
						}	
					}
				}else{
					$filename = $data['current_image'];
				}
				Category::where(['id'=>$id])->update(['name'=>$data['category_name'],'description'=>$data['desc'],'url'=>$data['url'],'status'=>$categorystatus,'image' => $filename]);
				return redirect('/admin/view-category')->with('flash_message_success','Categoria Editada satisfactoriamente');
			}
			$categoryDetails = Category::where(['id'=>$id])->first();
			$levels = Category::where(['parent_id' => 0])->get();
			return view('admin.categories.edit_category')->with(compact('categoryDetails','levels'));
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
	}

	public function deleteCategory($id = null){
		if(Session::has('adminSession')){
			if(!empty($id)){
				Category::where(['id'=>$id])->delete();
				return redirect('/admin/view-category')->with('flash_message_success','Categoria Eliminada satisfactoriamente');
			}
    	}else{
    		return redirect('/admin')->with('flash_message_error', 'Por favor inicia sesión para acceder');
    	}
	}


}
