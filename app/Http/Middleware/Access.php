<?php

namespace AllInOne\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Access
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
     public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect('/login');
        }
        return $next($request);
    }
}