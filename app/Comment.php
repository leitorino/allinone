<?php

namespace AllInOne;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function names(){
    	return $this->belongsTo('AllInOne\User','user_id');
    }
}
