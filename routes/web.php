<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get','post'],'/','FrontController@index');

Route::match(['get','post'],'admin','AdminController@login');

Auth::routes();

Auth::routes(['verify' => true]);

Route::post('/Login','UserController@login');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/categorias','FrontController@categorias');

//listing
Route::get('categoria/{url}', 'productController@products');

//listing
Route::get('categoria/{url}/{sort}', 'productController@productsOrdenados');

//listing
Route::get('todos-productos', 'productController@allProducts');

//product detail
Route::get('producto/{url}', 'productController@product');

//pagina del carrito
Route::match(['get', 'post'], '/carrito', 'ProductController@cart');

//carrito
Route::match(['get', 'post'], '/add-cart', 'ProductController@addtocart');

//buscar productos
Route::post('/search-products','ProductController@searchProducts');

//ajax guardar
Route::post('ajaxRequest', 'ProductController@ajaxRequestPost');

//ajax comentario
Route::post('sendcomment', 'ProductController@sendComment');

//ayuda
Route::get('ayuda', 'FrontController@ayuda');

//product detail
Route::get('carrito/borrar-producto/{url}', 'productController@deleteCartProduct');

//add favoritos
Route::post('agregar-favorito', 'ProductController@addFavorito');

//comprar
Route::match(['get', 'post'], '/pedido', 'ProductController@pedido')->name('comprar');

Route::group(['middleware'=>['access'],'middleware'=>['verified']],function(){
	
	// Order Review Page
	Route::match(['get','post'],'/order-review','productController@orderReview');
	// Place Order
	Route::match(['get','post'],'/place-order','productController@placeOrder');
	//paypal
	Route::get('/paypal','ProductController@paypal')->name('paypal');
	//thanks paypal
	Route::get('/thanks','ProductController@thanksPaypal')->name('thanks');
	//crm 
	Route::get('/crm','ProductController@crm')->name('crm');
	//cancel paypal
	Route::get('/cancelpaypal','ProductController@cancelPaypal')->name('cancel');
	// Users Orders Page
	Route::get('/pedidos','productController@userOrders');
	// User Ordered Products Page
	Route::get('/pedidos/{id}','productController@userOrderDetails');
	//favorites
	Route::get('/favoritos','productController@favoritos');
	//delete favorities
	Route::match(['get','post'],'/delete-favorite/{id}', 'ProductController@deleteFavorito');
	//add-ranking
	Route::post('/calificar-prod','productController@calificarProd');
	//pay paypal
	Route::post('/paypal-api','PaymentController@payWithpaypal');
});

//Admin routeste

Route::group(['prefix' => 'admin',], function(){
	Route::get('/dashboard','AdminController@dashboard');

	Route::get('/logout','AdminController@logout');

	Route::get('/settings', 'AdminController@settings');

	Route::get('/check-pwd', 'AdminController@chkPassword');

	Route::match(['get','post'],'/update-pwd', 'AdminController@updatePassword');

		//-admin categories routes

	Route::match(['get','post'],'/add-category', 'CategoryController@addCategory');

	Route::get('/view-category', 'CategoryController@viewCategory');

	Route::match(['get','post'],'/edit-category/{id}', 'CategoryController@editCategory');

	Route::match(['get','post'],'/delete-category/{id}', 'CategoryController@deleteCategory');

		//-admin products routes

	Route::match(['get','post'],'/add-product', 'ProductController@addProduct');

	Route::get('/view-product', 'ProductController@viewProduct');

	Route::match(['get','post'],'/edit-product/{id}', 'ProductController@editProduct');

	Route::get('/delete-product-image/{id}', 'ProductController@deleteProductImage');

	Route::match(['get','post'],'/delete-product/{id}', 'ProductController@deleteProduct');

		//-admin products attributes routes

	Route::match(['get','post'],'/add-attribute/{id}', 'ProductController@addAttributes');
	Route::match(['get','post'],'/add-images/{id}', 'ProductController@addImages');

		//-admin user route

	Route::get('/view-users', 'UserController@viewUsers');

	Route::match(['get','post'],'/edit-user/{id}', 'UserController@editUser');

		//- Admin Orders Routes
	Route::get('/view-orders','ProductController@viewOrders');

		//- Admin Order Details Route
	Route::get('/view-order/{id}','ProductController@viewOrderDetails');

		//- Update Order Status
	Route::post('/update-order-status','ProductController@updateOrderStatus');

		//- delete orders without pay
	Route::get('/delete-orders','ProductController@deleteOrders');

		//-banners route

	Route::match(['get','post'],'/add-banner', 'BannersController@addBanner');

	Route::get('/view-banners', 'BannersController@viewBanners');

	Route::get('/delete-banner/{id}', 'BannersController@deleteBanner');

	Route::match(['get','post'],'/edit-banner/{id}', 'BannersController@editBanner');

	//-publicidad route

	Route::match(['get','post'],'/add-publicidad', 'PublicidadController@addpublicidad');

	Route::get('/view-publicidad', 'PublicidadController@viewpublicidad');

	Route::get('/delete-publicidad/{id}', 'PublicidadController@deletepublicidad');

	Route::match(['get','post'],'/edit-publicidad/{id}', 'PublicidadController@editpublicidad');

		//-Comments
	Route::get('/view-comments', 'ProductController@viewComment');

	Route::post('/delete-comment', 'ProductController@deleteComment');


	//-------------------------------------------------------------------------------------------------
});




