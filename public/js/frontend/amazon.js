$(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

    //replace main image with alternative image
    $('.changeimage').hover(function(){
        var image = $(this).attr('src');
        $('.mainimage').attr('src',image);
    });

    $('.image-big2').hover(function(){
        var image2 = $(this).attr('href');
        $('.image-big').attr('href',image2);
    });

    // Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom();

    // Setup thumbnails example
    var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    $('.thumbnails').hover(function(e) {
        
        var $this = $(this);

        e.preventDefault();

        // Use EasyZoom's `swap` method
        api1.swap($this.data('standard'), $this.attr('href'));
    });


    var preciob = ($('#price').html());
  
    $('#plan').change(function(){
        if($('#plan').val()==1){
            $('#img-plan1').show(); 
            $('#img-plan2').hide();
            $('#img-plan3').hide();
            $('.text-emprendedor').show(); 
            $('.text-empresas').hide();
            $('.text-personalizado').hide();
            $('#price').replaceWith('<h1 id="price">' + preciob + '</h1>'); 
        }else if($('#plan').val()==2){
            $('#img-plan1').hide(); 
            $('#img-plan2').show();
            $('#img-plan3').hide();
            $('.text-emprendedor').hide(); 
            $('.text-empresas').show();
            $('.text-personalizado').hide(); 
            var nprecio = preciob * 2;
            $('#price').replaceWith('<h1 id="price">' + nprecio + '</h1>');
        }else if($('#plan').val()==3){
            $('#img-plan1').hide(); 
            $('#img-plan2').hide();
            $('#img-plan3').show();
            $('.text-emprendedor').hide(); 
            $('.text-empresas').hide();
            $('.text-personalizado').show(); 
            var nprecio = preciob * 3;
            $('#price').replaceWith('<h1 id="price">' + nprecio + '</h1>');
        }
    });

  $('body #products-list').on('change', '.plan2', function(e){
    var nid = parseInt($('.plan2').index(this));
    var preciob2 = ($('.preciob:eq('+nid+')').html());
    var cantprod = parseInt($('.scant:eq('+nid+')').val());

    if($(this).val()==1){
        $('.img-plan1:eq('+nid+')').show(); 
        $('.img-plan2:eq('+nid+')').hide();
        $('.img-plan3:eq('+nid+')').hide();
        $('.subtotal-ind:eq('+$('.plan2').index(this)+')').replaceWith('<h2 class="subtotal-ind" style="display: inline;">' + preciob2*cantprod + '</h2>');
        $('.price:eq('+$('.plan2').index(this)+')').replaceWith('<h2 class="text-center price" style="color: #646464; display: inline">' + preciob2 + '</h2>'); 
        $('.dprice:eq('+$('.plan2').index(this)+')').replaceWith('<h2 class="dprice" style="color: #646464; display: inline"> $ </h2>'); 
    }else if($(this).val()==2){
        $('.img-plan1:eq('+nid+')').hide(); 
        $('.img-plan2:eq('+nid+')').show();
        $('.img-plan3:eq('+nid+')').hide(); 
        var nprecio = preciob2 * 2;
        $('.subtotal-ind:eq('+$('.plan2').index(this)+')').replaceWith('<h2 class="subtotal-ind" style="display: inline;">' + nprecio*cantprod + '</h2>');
        $('.price:eq('+nid+')').replaceWith('<h2 class="text-center price" style="color: #d18b19; display: inline">' + nprecio + '</h2>');
        $('.dprice:eq('+nid+')').replaceWith('<h2 class="dprice" style="color: #d18b19; display: inline"> $ </h2>');
    }else if($(this).val()==3){
        $('.img-plan1:eq('+nid+')').hide(); 
        $('.img-plan2:eq('+nid+')').hide();
        $('.img-plan3:eq('+nid+')').show(); 
        var nprecio = preciob2 * 3;
        $('.subtotal-ind:eq('+$('.plan2').index(this)+')').replaceWith('<h2 class="subtotal-ind" style="display: inline;">' + nprecio*cantprod + '</h2>');
        $('.price:eq('+nid+')').replaceWith('<h2 class="text-center price" style="color: #0058A3; display: inline">' + nprecio + '</h2>');
        $('.dprice:eq('+nid+')').replaceWith('<h2 class="dprice" style="color: #0058A3; display: inline"> $ </h2>');
    }



    //AJAX

        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

        e.preventDefault();

        var id = $('.idp:eq('+nid+')').val();

        var plan = $('.plan2:eq('+nid+')').val();

        $.ajax({

           type:'POST',

           url:'/ajaxRequest',

           data:{id:id, plan:plan},

           success:function(data){

              // alert(data.success);

           }

        });
    
   });
    $('#plan').tooltip();
    setTimeout("$('#plan').tooltip('show')",2000)
    setTimeout("$('#plan').tooltip('hide')",5000)
    $('[data-toggle="tooltip"]').tooltip({animation: true, delay: {show: 500, hide: 1000}});

});

  var cont = 0;
$('.items').each(function (){

    $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

        // e.preventDefault();

        var id = $('.idp:eq('+cont+')').val();


        $.ajax({

           type:'POST',

           url:'/ajaxRequest',

           data:{id:id},

           success:function(data){

              // alert(data.success);

           }

        });

        cont = cont + 1;
    
});

  var suma = 0;
  var cont = 0;
  var cont2 = 0;
  $('.price').each(function (){
    var cantprod = parseInt($('.scant:eq('+cont+')').val());
    var precio = parseInt($(this).text()) * cantprod;
    suma = suma + precio;
    cont = cont + 1;
    cont2 = cont2 + (1 * cantprod);
  });

  $('.subtotal1').text('Sub Total: ('+ cont2 +' Productos)');
  $('.subtotal2').text('$'+suma);
  $('.total-orden').replaceWith('<span class="total-orden">'+suma+'</span>');


$('.scant').change(function(e){
    var nid = parseInt($('.scant').index(this));
    var preciob2 = ($('.preciob:eq('+nid+')').html());
    var cantprod = parseInt($('.scant:eq('+nid+')').val());
    $('.subtotal-ind:eq('+nid+')').replaceWith('<h2 class="subtotal-ind" style="display: inline;">' + preciob2*cantprod + '</h2>');


    //Ajax

        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

        e.preventDefault();

        var id = $('.idp:eq('+nid+')').val();

        var cant = $('.scant:eq('+nid+')').val();

        $.ajax({

           type:'POST',

           url:'/ajaxRequest',

           data:{id:id, cant:cant},

           success:function(data){

              // alert(data.success);

           }

        });

});



$('body').change(function(){

    var suma = 0;
    var cont = 0;
    var cont2 = 0;
    $('.price').each(function (){
    var cantprod = parseInt($('.scant:eq('+cont+')').val());
    var precio = parseInt($(this).text()) * cantprod;
    suma = suma + precio;
    cont = cont + 1;
    cont2 = cont2 + (1 * cantprod);
    });

    $('.subtotal1').text('Sub Total: ('+ cont2 +' Productos)');
    $('.subtotal2').text('$'+suma);
    $('.total-orden').replaceWith('<span class="total-orden">'+suma+'</span>');

});

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    // autoWidth:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:false
        },
        600:{
            items:4,
            nav:false
        },
        1000:{
            items:6,
            nav:true,
            loop:false
        }
    }
});

$('.agg-comment').click(function(){

    $( ".comment" ).show();
    $('.agg-comment').hide();

});

var comment_cont = 0;
$('.send-comment').click(function(e){
    var comentario = $('.comentario').val();
    var user_id = $('.user-id').val();
    var product_id = $('.product-id').val();
    var username = $('.user-name').val();

    $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

        e.preventDefault();

        $.ajax({

           type:'POST',

           url:'/sendcomment',

           data:{user_id:user_id, product_id:product_id, comentario,comentario},

           success:function(data){

            var f = new Date();

              $('.comments').append('<div class="m-3 comment-border'+comment_cont+'" style="border: 1px solid #bfbdbd"></div>');
              $('.comment-border'+comment_cont+'').append('<div class="m-3 comment-div'+comment_cont+'"></div>');
              $('.comment-div'+comment_cont+'').append('<div class="fas fa-user-circle fa-2x" style="color: grey"></div>');
              $('.comment-div'+comment_cont+'').append('<h4 style="display: inline-block;" class="comment-username'+comment_cont+' ml-1"></h4>');
              $('.comment-username'+comment_cont+'').append(username);
              $('.comment-div'+comment_cont+'').append('<h5 class="comment-date'+comment_cont+'"></h5>');
              $('.comment-date'+comment_cont+'').append(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate());
              $('.comment-div'+comment_cont+'').append('<p class="p-comment'+comment_cont+'"></p>');
              $('.p-comment'+comment_cont+'').append(comentario);
              comment_cont = comment_cont + 1;
              $('.comentario').val("");

           }

        });

});

var fav_cont = 0;
$('.add-fav').click(function(e){

    var nid = parseInt($('.add-fav').index(this));

    var product_id = $('.product-id:eq('+nid+')').val();

    $.ajaxSetup({ 

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

        e.preventDefault();

        $.ajax({

           type:'POST',

           url:'/agregar-favorito',

           data:{product_id:product_id},

           success:function(data){

           
            $('.fav-alert').append('<div class="col-12 alert alert-success alert-block fav-alert2'+fav_cont+'"></div>');
            $('.fav-alert2'+fav_cont+'').append('<button type="button" class="close" data-dismiss="alert">x</button>');
            $('.fav-alert2'+fav_cont+'').append('<strong> Producto Agregado a Favoritos </strong>');
            fav_cont = fav_cont + 1;

           }

        });

});

$('.ord-prod').change(function(){

    window.location=$('.ord-prod').val();

});

var ranking_cont = 0;
$('.califica-prod').change(function(e){

    var calificacion = $('.califica-prod').val();
    var product_id = $('.product_id_cal').val();

    $.ajaxSetup({ 

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

        e.preventDefault();

        $.ajax({

           type:'POST',

           url:'/calificar-prod',

           data:{calificacion:calificacion, product_id:product_id},

           success:function(data){

           $('.fav-alert').append('<div class="col-12 alert alert-success alert-block fav-alert2'+ranking_cont+'"></div>');
            $('.fav-alert2'+ranking_cont+'').append('<button type="button" class="close" data-dismiss="alert">x</button>');
            $('.fav-alert2'+ranking_cont+'').append('<strong> Producto Valorado </strong>');
            ranking_cont = ranking_cont + 1;
           

           }

        });

});

$('.btn-comprar').click(function(e){

  $('.total-orden').replaceWith('<span class="total-orden">'+suma+'</span>');

    $.ajaxSetup({ 

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

        e.preventDefault();

        var json = $( this ).serialize();
        var peticion = $.getJSON( "/pedido", json, function(data){
            console.log( "Petición enviada" );
        }).done(function(data){
            console.log(data);
            $('.num-orden').replaceWith('<span class="num-orden">'+data+'</span>');
        }).fail(function(){
            console.log( "error en la petición ajax" );
        });

});

// if($("[name='First Name']").val()!=null){
//             $( "#crm" ).submit();
//         }




