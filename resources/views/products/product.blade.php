@extends('layouts.publicLayout.public_design')
@section('content')   
	<div>
		<div class="row fav-alert">
                            
        </div>
		<div class="row m-5">

			<div class="col-1">
				<ul class="thumbnails">
					<li class="mt-2">
						<a href="{{ url('images/backend_images/products/big/'.$productDetails->image) }}" data-standard="{{ asset('images/backend_images/products/medium/'.$productDetails->image) }}" class="image-big2">
							<img src="{{ asset('images/backend_images/products/medium/'.$productDetails->image) }}" height="10%" width="80%" alt="" class="img-fluid changeimage altimage">
						</a>
					</li>
					@foreach($images as $image)
						<li class="mt-2">
							<a href="{{ url('images/backend_images/products/big/'.$image->image) }}" data-standard="{{ asset('images/backend_images/products/medium/'.$image->image) }}" class="image-big2">
								<img src="{{ asset('images/backend_images/products/medium/'.$image->image) }}" height="10%" width="80%" alt="" class="img-fluid changeimage altimage">
							</a>
						</li>
					@endforeach
				</ul>
			</div>

			<div class="col-md-4 col-sm-10">
				<div class="container md-2 mt-4 mb-4">
					<div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
						<a href="{{ url('images/backend_images/products/big/'.$productDetails->image) }}" class="image-big">
							<img src="{{ asset('images/backend_images/products/medium/'.$productDetails->image) }}" alt="" class=" mainimage img-fluid">
						</a>
					</div>	
				</div>
			</div>

			<div class="col-md-7 col-sm-12">
				<form name="addtocartForm" id="addtocartForm" action="{{ url('add-cart') }}" method="post">{{ csrf_field() }}
					<div class="row">
						<meta name="csrf-token" content="{{ csrf_token() }}" />
						<input type="hidden"name="product_id" value="{{ $productDetails->id }}" class="product-id">
						<input type="hidden"name="product_name" value="{{ $productDetails->product_name }}">
						<input type="hidden"name="product_code" value="{{ $productDetails->product_code }}">
						<input type="hidden"name="precio" value="{{ $productDetails->precio }}">
						<div class="col-md-8 col-sm-12">
							<div class="row">
								<h1 class="text-center">
									{{$productDetails->product_name}}
								</h1>
							</div>
							<div class="row mt-1 mb-2">
								@for ($i = 0; $i < 5; $i++)
									@if($i < $starsprom)
										<span class="fa fa-star fa-lg borderstar mr-1" style="color: #d18b19"></span>
									@else
										<span class="fa fa-star fa-lg borderstar mr-1" style="color: white;"></span>
									@endif
								@endfor
								<a href="#ranking">{{$cont}} Opiniones de clientes</a>
								&nbsp|&nbsp
								<a href="#comentarios">Comentarios</a>
							</div>
							<hr>
							<div class="row" style="background-color: white;">
								<h4 style="vertical-align: middle;">
									Precio:&nbsp 
								</h4>
								
								<h1 id="price">
									{{$productDetails->precio}}
								</h1>
								<h1>$</h1>
							</div>
							<br>
							<div class="row mt-1">
								<div class="col-12">
									<h4>Descripción:</h4>
									{{$productDetails->description}}
								</div>
							</div>
							<div class="row mt-2 form-group">
								<div style="margin-right: 1%; width: 60%;">
									<img src="{{ asset('images/frontend_images/p-emprendedor.gif') }}" class="img-fluid img-plan"  id="img-plan1">
								    <img src="{{ asset('images/frontend_images/p-dedicado.gif') }}" class="img-fluid img-plan"  id="img-plan2" style="display: none;">
								    <img src="{{ asset('images/frontend_images/p-empresa.gif') }}" class="img-fluid img-plan" id="img-plan3" style="display: none;">
								</div>
							    
								<select class="form-control col-4" name="plan" id="plan" data-toggle="tooltip" data-html="true" title="Selecciona un Plan">
									<option disabled selected>
										Cambiar Plan
									</option>
									<option value="1">
										Emprendedor
									</option>
									<option value="2">
										Dedicado
									</option>
									<option value="3">
										Empresa
									</option>
								</select>
							</div>
							<div class="row mt-2 form-group">
								<h5 style="text-align: center;">Cantidad:</h5>
								<select class="form-control col-2 offset-1" name="cantidad">
									<option value="1">
										1
									</option>
									<option value="2">
										2
									</option>
									<option value="3">
										3
									</option>
									<option value="4">
										4
									</option>
									<option value="5">
										5
									</option>
								</select>
							</div>
							<div class="row mt-2">
								<div class="col-12">
									<h4>Descripción Detallada:</h4>
									{!!$productDetails->descriptiondetallado!!}
								</div>
							</div>
						</div>

						<div class="col-md-4 col-sm-12 mt-4">
							<p class="font-weight-bold " style="color:blue;display: inline-block;">Compartir</p>
							<a href="facebook.com" class="ml-2"><span class="fab fa-facebook-f fa-2x d-inline-block"></span></a>
							<a href="twitter.com" class="ml-2"><span class="fab fa-twitter fa-2x d-inline-block"></span></a>
							<a href="instagram" class="ml-2"><span class="fab fa-instagram fa-2x d-inline-block"></span></a>
							<div class="row" style="background-color: #EEEEEE; border-radius: 5px;">
								<div class="col-12 mt-4 text-center">
									<button type="submit" class="btn col-form-label-sm btn-carrito">
										<div style="display: inline-block; left: 0%;">
											<span class="fas fa-shopping-cart"></span>
										</div>
										<div style="display: inline-block;">
											<p class="font-weight-bold">Agregar al Carrito</p>
										</div>
									</button>
								</div>
								<div class="col-12 mt-3 mb-4 text-center">
									<span class="btn col-form-label-sm btn-carrito add-fav">
										<div style="display: inline-block; left: 0%;">
											<span class="fas fa-heart"></span>
										</div>
										<div style="display: inline-block;">
											<p class="font-weight-bold">Agregar a Favoritos</p>
										</div>
									</span>			
								</div>
							</div>

							<div class="row mt-4" style="background-color: #68BCFF;">
								<div>
									<a href="{{url('/categoria/Ofertas')}}">
										<img src="{{ asset('images/backend_images/products/medium/'.$produc_promo->image) }}" class="img-fluid">
									</a>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<hr>
		<div class="row mt-4 ml-4 mr-4">

			<h3 class="m-3">Productos Relacionados Con Este Articulo</h3>


			<div class="col-12">
				<div class="row">

					<div class="owl-carousel col-12">

						@foreach($products_rel as $produc_rel)
						<div>
							<a href="{{ url('/producto/'.$produc_rel->id) }}"><img src="{{ asset('images/backend_images/products/medium/'.$produc_rel->image) }}" class="img-fluid"><p style="font-size: 1rem; font-weight: bold;">{{$produc_rel->product_name}}</p></a>
							<h4>${{$produc_rel->precio}}</h4>
						</div>
						@endforeach

					</div>

		    	</div>
		    </div>
		</div>
		<hr>
		<div class="row mt-4 ml-4 mr-4 mb-2">
			<div class="container" >
				<div class="row">
					<div class="col-md-3">
						<div id="ranking" style="border: 1px solid #bfbdbd">
							<div class="m-3">
								<h3>{{$cont}} Opiniones De Clientes</h3>
								@for ($i = 0; $i < 5; $i++)
									@if($i < $starsprom)
										<span class="fa fa-star fa-lg borderstar mr-1" style="color: #d18b19"></span>
									@else
										<span class="fa fa-star fa-lg borderstar mr-1" style="color: white;"></span>
									@endif
								@endfor
								<h6 style="display: inline-block;" class="mb-3 mt-2">{{$starsprom}} de 5 Estrellas</h6>
								<br>
								<h5 style="display: inline-block;">5 Estrellas</h5>
								<meter min="0" max="100" value="{{($starsind[4]/$cont2)*100}}">
								</meter>
								<p style="display: inline-block;">{{($starsind[4]/$cont2)*100}}%</p>
								<br>
								<h5 style="display: inline-block;">4 Estrellas</h5>
								<meter min="0" max="100" value="{{($starsind[3]/$cont2)*100}}">
								</meter>
								<p style="display: inline-block;">{{($starsind[3]/$cont2)*100}}%</p>
								<br>
								<h5 style="display: inline-block;">3 Estrellas</h5>
								<meter min="0" max="100" value="{{($starsind[2]/$cont2)*100}}">
								</meter>
								<p style="display: inline-block;">{{($starsind[2]/$cont2)*100}}%</p>
								<br>
								<h5 style="display: inline-block;">2 Estrellas</h5>
								<meter min="0" max="100" value="{{($starsind[1]/$cont2)*100}}">
								</meter>
								<p style="display: inline-block;">{{($starsind[1]/$cont2)*100}}%</p>
								<br>
								<h5 style="display: inline-block;">1 Estrellas</h5>
								<meter min="0" max="100" value="{{($starsind[0]/$cont2)*100}}">
								</meter>
								<p style="display: inline-block;">{{($starsind[0]/$cont2)*100}}%</p>
							</div>
						</div>	
					</div>
					

					<div class="col-md-7 offset-md-1 col-sm-12" id="comentarios">
						<center>
							<h2>Comentarios</h2>
						</center>
						@foreach($comments as $comment)
						<div>
							<div class="m-3" style="border: 1px solid #bfbdbd">
								<div class="m-3">
									<div class="fas fa-user-circle fa-2x" style="color: grey"></div>		
									<h4 style="display: inline-block;">{{$comment->names->username}}</h4>
									<h5>{{$comment->created_at}}</h5>
									<p>{{$comment->comment}}</p>
								</div>
							</div>
						</div>
						@endforeach
						<div class="comments">
						</div>
						@guest
						<center>
							<h4 class="m-3">Inicia Sesión Para Comentar</h4>
						</center>
						@else
						<meta name="csrf-token" content="{{ csrf_token() }}" />
						<input type="hidden" class="user-id" value="{{Auth::user()->id}}">
						<input type="hidden" class="user-name" value="{{Auth::user()->username}}">
						<input type="hidden" class="product-id" value="{{$id}}">
						<div class="comment m-3" style="display: none;">
							<label>Escribe Tu Comentario:</label>
							<textarea class="form-control comentario"></textarea>
							<center><input type="button" class="btn btn-outline-dark m-3 send-comment" value="Enviar"></center>
							
						</div>
						<button class="agg-comment btn btn-light btn-outline-dark m-3">Escribir mi opinión</button>
						@endguest
						
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection