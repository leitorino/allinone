@extends('layouts.publicLayout.public_design')
@section('content')  
<?php use AllInOne\Order; ?>

<section id="do_action">
	<div class="container">
		<div class="heading" align="center">
			<h3>TU ORDEN HA SIDO TOMADA</h3>
			<p>Tu numero de orden es: 0000{{ Session::get('order') }} y el total a pagar es:  {{ Session::get('total') }}$USD</p>
			<form action="/paypal-api" method="post">{{ csrf_field() }}
				<input type="hidden" name="total" value="{{ Session::get('total') }}" >
				<button class="btn btn-primary">Proceder Al Pago Con Paypal</button>
			</form>
		</div>
	</div>
</section>

@endsection