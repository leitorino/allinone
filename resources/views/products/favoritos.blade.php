@extends('layouts.publicLayout.public_design')
@section('content') 

@if(Session::has('flash_message_success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{!! session('flash_message_success') !!} </strong>
    </div>          
@endif 
  	
<section id="do_action">

		<div class="heading  p-2" align="center">
			<table id="example" class="table table-striped table-bordered text-center">
                <thead>
                    <tr>
                        <th class="pp0">Imagen del Producto</th>
                        <th class="pp0">Nombre del Producto</th>
                        <th class="pp0">Descripción</th>
                        <th class="pp0">Precio Base</th>
                        <th class="pp0">Opción</th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($products as $prod)
                    <tr >
                        <td width="20%" class="pp0"><img src="{{ asset('images/backend_images/products/medium/'.$prod->prod->image) }} " class="img-fluid"></td>
                        <td width="15%" class="pp0">{{$prod->prod->product_name}}</td>
                        <td class="text-left" class="pp0">{{$prod->prod->description}}</td>
                        <td width="10%" class="pp0">{{$prod->prod->precio}}</td>
                        <td width="15%" class="pp0">
                            <div>
                                <a href="{{url('producto/'.$prod->prod->id)}}">Ver</a>
                            </div>
                            <div>
                                <a href="{{url('delete-favorite/'.$prod->id)}}">Eliminar</a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
		</div>

</section>

@endsection