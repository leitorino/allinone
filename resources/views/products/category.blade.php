@extends('layouts.publicLayout.public_design')
@section('content')   
    <div class="fondo">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="shop_sidebar_area">
                        @if(($sub_categories)!=[])
                        <!-- ##### Single Widget ##### -->
                        <div class="widget catagory mb-50">
                            <!-- Widget Title -->
                            <h6 class="widget-title mb-30">Sub Categorias</h6>

                            <!--  Catagories  -->
                            <div class="catagories-menu">
                                <ul id="menu-content2" class="menu-content collapse show">
                                    
                                    @foreach($sub_categories as $cat)

                                        <li><a href="{{ url('/categoria/'.$cat->url) }}" class="a-black">{{$cat->name}}</a></li>

                                    @endforeach

                                </ul>
                            </div>
                        </div>
                        @else

                        <div class="widget catagory mb-50">
                            <!-- Widget Title -->
                            <h6 class="widget-title mb-30">Categorias</h6>

                            <!--  Catagories  -->
                            <div class="catagories-menu">
                                <ul id="menu-content2" class="menu-content collapse show">
                                    
                                    @foreach($categories as $cat)

                                        <li><a href="{{ url('/categoria/'.$cat->url) }}" class="a-black">{{$cat->name}}</a></li>

                                    @endforeach

                                </ul>
                            </div>
                        </div>

                        @endif
                    </div>
                    <div class="publicidad">
                        @if(isset($publicidad))
                        <a href="{{url('/categoria/Ofertas')}}">
                            <img class="d-block w-100" src="{{ asset('images/frontend_images/publicidad/'.$publicidad->image) }}">
                        </a>
                        @endif
                    </div>
                </div>

                <div class="col-12 col-md-8 col-lg-9">
                    <div class="shop_grid_product_area">
                        <div class="row">
                            <div class="col-12">
                                <div class="product-topbar text-center align-items-center justify-content-between">
                                    <div id="s-result-info-bar-content" class="row">
                                        <div class="col-md-6 col-sm-12 text-center">
                                            <div class="s-first-column">
                                                <div class="a-section a-spacing-small a-spacing-top-small">
                                                    @if($cant_prod<(24))
                                                        <span id="s-result-count">1-{{$cant_prod}} de {{$cant_prod}} @if (isset($categoryDetails)) resultados para 
                                                        
                                                        <span>
                                                            <span class="a-color-state a-text-bold">{{$categoryDetails->name}}</span>
                                                        </span>
                                                        @endif
                                                    </span>
                                                    @else
                                                        <span id="s-result-count">1-24 de {{$cant_prod}} resultados para 
                                                        @if(isset($categoryDetails))
                                                        <span>
                                                            <span class="a-color-state a-text-bold">{{$categoryDetails->name}}</span>
                                                        </span>
                                                        @endif
                                                        
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="a-span4 a-spacing-none a-span-last col-md-6 col-sm-12">
                                            <div class="a-row a-spacing-micro a-spacing-top-micro">
                                                <div class="s-last-column">
                                                    <form id="searchSortForm" method="get" action="/s/ref=lp_16225007011_st" class="s-inline-form a-spacing-none">
                                                        <input type="hidden" name="rh" value="n:16225007011">
                                                        <input type="hidden" name="qid" value="1546273464">
                                                        @if(isset($categoryDetails))
                                                        <span id="sort_by_text" class="a-size-base">Ordenar Por&nbsp;</span>
                                                        <select class="a-spacing-top-mini ord-prod" style="vertical-align: baseline;" name="sort" id="sort">
                                                            <option value="{{url('/categoria/'.$url.'/dest')}}">Destacado</option>
                                                            <option value="{{url('/categoria/'.$url.'/menor')}}"><a href="">Precio: Bajo a Alto</option>
                                                            <option value="{{url('/categoria/'.$url.'/mayor')}}">Precio: Alto a Bajo</option>
                                                            <option value="{{url('/categoria/'.$url.'/ultimas')}}">Últimas llegadas</option>
                                                        </select>
                                                        @endif
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row fav-alert">
                            
                        </div>
                        <div class="row">

                            @foreach($products as $product)

                                <!-- Single Product -->
                                <div class="col-12 col-sm-6 col-lg-4">
                                    <div class="single-product-wrapper">
                                        <!-- Product Image -->
                                        <div class="product-img">
                                            <a href="{{ url('/producto/'.$product->id) }}">
                                                <img src="{{ asset('images/backend_images/products/medium/'.$product->image) }}" alt="">
                                            </a>

                                            <!-- Favourite -->
                                            @guest
                                            @else
                                            <meta name="csrf-token" content="{{ csrf_token() }}" />
                                            <div class="product-favourite">
                                                <a class="favme fa fa-heart add-fav"></a>
                                            </div>
                                            <input type="hidden" value="{{$product->id}}" class="product-id">
                                            @endguest
                                            
                                        </div>

                                        <!-- Product Description -->
                                        <div class="product-description pl-3">                                            <a href="{{ url('/producto/'.$product->id) }}">
                                                <h2 style="color: #c5a239;">${{$product->precio}}</h2>
                                            </a>
                                            <h3 class="mt-1"  style="color: grey;">{{$product->product_name}}</h3>

                                            <!-- Hover Content -->
                                            <div class="hover-content">
                                                <!-- Add to Cart -->
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                        </div>
                    </div>
                </div>
                <!-- Pagination -->
                    <div class="mt-3 mb-3 offset-md-5 col-md-5 col-sm-12">

                            @if ($products->hasPages())
                                <ul class="pagination pagination">
                                    {{-- Previous Page Link --}}
                                    @if ($products->onFirstPage())
                                        <button class="disabled btn bnt-primary"><span>«</span></button>
                                    @else
                                        <a href="{{ $products->previousPageUrl() }}" rel="prev"><button class=" btn bnt-primary">«</button></a>
                                    @endif

                                    @if($products->currentPage() > 3)
                                        <a href="{{ $products->url(1) }}"><button class="hidden-xs  btn bnt-primary">1</button></a>
                                    @endif
                                    @if($products->currentPage() > 4)
                                        <button class="btn btn-primary"><span>...</span></button>
                                    @endif
                                    @foreach(range(1, $products->lastPage()) as $i)
                                        @if($i >= $products->currentPage() - 2 && $i <= $products->currentPage() + 2)
                                            @if ($i == $products->currentPage())
                                                <button class="active btn bnt-primary"><span>{{ $i }}</span></button>
                                            @else
                                                <button class=" btn bnt-primary"><a href="{{ $products->url($i) }}">{{ $i }}</a></button>
                                            @endif
                                        @endif
                                    @endforeach
                                    @if($products->currentPage() < $products->lastPage() - 3)
                                        <button class="btn bnt-primary"><span>...</span></button>
                                    @endif
                                    @if($products->currentPage() < $products->lastPage() - 2)
                                        <button class="hidden-xs  btn bnt-primary"><a href="{{ $products->url($products->lastPage()) }}">{{ $products->lastPage() }}</a></button>
                                    @endif

                                    {{-- Next Page buttonnk --}}
                                    @if ($products->hasMorePages())
                                        <button class="btn bnt-primary"><a href="{{ $products->nextPageUrl() }}" rel="next">»</a></button>
                                    @else
                                        <button class="disabled"><span>»</span></button>
                                    @endif
                                </ul>
                            @endif

                    </div>
            </div>
        </div>        
    </div>

@endsection