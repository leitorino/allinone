@extends('layouts.publicLayout.public_design')
@section('content')   


@if(Session::has('flash_message_success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{!! session('flash_message_success') !!} </strong>
    </div>          
  	@endif
  	
<section id="do_action">

		<div class="heading p-2" align="center">
			<table id="example" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="text-center pp0" width="15%">ID de la Orden</th>
                        <th class="text-center pp0" width="15%">Productos Solicitados</th>
                        <th class="text-center pp0" width="20%">Precio Total</th>
                        <th class="text-center pp0" width="20%">Fecha de Creación de la Orden</th>
                        <th class="text-center pp0" width="30%">Estado</th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($orders as $order)
                    <tr>
                        <td class="pp0">{{ $order->id }}</td>
                        <td class="pp0">
                        	@foreach($order->orders as $pro)
                        		<a href="{{ url('/pedidos/'.$order->id) }}">{{ $pro->id }}</a><br>
                        	@endforeach
                        </td>
                        <td class="pp0">{{ $order->total }}</td>
                        <td class="pp0">{{ $order->created_at }}</td>
                        <td class="pp0">{{ $order->status }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
		</div>
        {!!$orders!!}

</section>

@endsection