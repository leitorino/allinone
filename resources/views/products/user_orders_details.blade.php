@extends('layouts.publicLayout.public_design')
@section('content')   

<div class="row fav-alert">
                            
</div>

<section id="do_action">
	<div class="container">
		<div class="heading" align="center">
			<table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
	            	<tr>
		                <th>Nombre del Producto</th>
		                <th>Precio Individual</th>
		                <th>Cantidad de Productos</th>
		            </tr>
		        </thead>
                <tbody>
		            @foreach($orderDetails->orders as $pro)
		            <?php 
		            foreach ($products as $prod) {
		              if($prod->id == $pro->product_id){
		                $name = $prod->product_name;
		                $precio =  $pro->plan * $prod->precio;
		              }
		            }
		            ?>
		              <tr>
		                  <td>{{ $name }}</td>
		                  <td>{{ $precio }}</td>
		                  <td>{{ $pro->cantidad }}</td>
		                  <td>
                            <meta name="csrf-token" content="{{ csrf_token() }}" />
                            <input type="hidden" value="{{$pro->product_id}}" class="product_id_cal">
                            <select class="form-control califica-prod" name="califica-prod" id="califica-prod">
                                <option disabled selected>
                                    Califica
                                </option>
                                <option value="1">
                                    1
                                </option>
                                <option value="2">
                                    2
                                </option>
                                <option value="3">
                                    3
                                </option>
                                <option value="4">
                                    4
                                </option>
                                <option value="5">
                                    5
                                </option>
                            </select>
                        </td>
		              </tr>
		              @endforeach
		          </tbody>
            </table>
		</div>
	</div>
</section>

@endsection