@extends('layouts.publicLayout.public_design')
@section('content') 
<div class="modal" id="pagar" tabindex="-1" role="dialog" aria-labelledby="pagarLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<h3>TU ORDEN HA SIDO TOMADA</h3>
		<h4>Tu numero de orden es: 0000<span class="num-orden"></span></h4>
		<h4>Total a pagar es:  <span class="total-orden"></span>$USD</h4>
		<form action="/paypal-api" method="post" >{{ csrf_field() }}
			<input type="hidden" name="total" value="{{ Session::get('total') }}" >
			<a href="{{ url('/cancelpaypal') }}"><button type="button" class="btn btn-secondary">Cancelar</button></a>
			<button class="btn btn-primary">Proceder Al Pago Con Paypal</button>
		</form>
      </div>
    </div>
  </div>
</div>
	@if(Session::has('flash_message_success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{!! session('flash_message_success') !!} </strong>
    </div>          
  	@endif
  	@if(Session::has('flash_message_error'))
        <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>{!! session('flash_message_error') !!} </strong>
        </div>          
    @endif
		<div class="row">
			<div class="col-md-9 col-sm-12 mt-4 p-5">
				<div class="row">
					<table class="table">
					  <thead>
					    <tr>
					    	<th scope="col">
					      		<h2>Carrito</h2>
					  		</th>
					  		<th scope="col" class="text-center">
					      		<span>Precio Unitario</span>
					  		</th>
					  		<th scope="col" class="text-center">
					      		<span>Cant.</span>
					  		</th>
					  		<th>
					  			<span>Precio</span>
					  		</th>
					    </tr>
					  </thead>
					  <tbody id="products-list">
					  	<?php $cont=1; $cant=0;?>
					  	@foreach($usercart as $item)
					  	@foreach($products as $product)
					  	@if($item->product_id == $product->id)
					    <tr class="items">
					      <td>
					      	<meta name="csrf-token" content="{{ csrf_token() }}" />
					      	<input type="hidden" class="idp"  value="{{$item->id}}">
					      	<div class="row">
					      		<div class="col-md-3 col-sm-12">
					      			<a href="{{ url('/producto/'.$product->id) }}">
						      			<img src="{{ asset('images/backend_images/products/big/'.$product->image) }}" class="img-fluid">
						      		</a>
						      	</div>
						      	<div class="col-md-9 col-sm-12">
						      		<a href="{{ url('/producto/'.$product->id) }}" class="prod-title">{{$product->product_name}}</a><br>
						      		<a href="{{ url('/carrito/borrar-producto/'.$item->id) }}">Eliminar </a>
						      		|
						      		<a href=""> Agregar a Favorito</a>
						      		<div class="row mt-2 form-group">
						      			
						      			<div class="col-md-7 col-sm-12">
						      				<img src="{{ asset('images/frontend_images/p-emprendedor.png') }}" class="img-fluid img-plan img-plan1"  @if($item->plan != 1) style="display: none" @endif >
										    <img src="{{ asset('images/frontend_images/p-dedicado.png') }}" class="img-fluid img-plan img-plan2"  @if($item->plan != 2) style="display: none" @endif>
										    <img src="{{ asset('images/frontend_images/p-empresa.png') }}" class="img-fluid img-plan img-plan3" @if($item->plan != 3) style="display: none" @endif>
						      			</div>
						      			
									    
										<select class="form-control plan2 col-md-4 col-sm-12 ml-2" name="plan">
											<option disabled selected>
												Cambiar Plan
											</option>
											<option value="1">
												Emprendedor
											</option>
											<option value="2">
												Dedicado
											</option>
											<option value="3">
												Empresa
											</option>
										</select>
									</div>
						      	</div>
					      	</div>
					      </td>
					      <td style="width: 20%;" class="text-center">
					      	<h2 class="dprice" @if($item->plan == 1) style="color: #646464; display: inline" @elseif($item->plan == 2) style="color: #d18b19; display: inline" @elseif($item->plan == 3) style="color: #0058A3; display: inline" @endif>$</h2>
					      	<h2 class="price" @if($item->plan == 1) style="color: #646464; display: inline" @elseif($item->plan == 2) style="color: #d18b19; display: inline" @elseif($item->plan == 3) style="color: #0058A3; display: inline" @endif >@if($item->plan == 1){{$product->precio}} @elseif($item->plan == 2){{$product->precio*2}} @elseif($item->plan == 3){{$product->precio*3}} @endif</h2>
					      	<span style="display: none;" class="preciob">{{$product->precio}}</span>
					      </td>
					      <td style="width: 20%;">
					      	<center>
						      	<select class="form-control scant text-center" style="width: 60px;" name="cantidad">
									<option value="1" @if($item->cantidad == 1) selected @endif>
										1
									</option>
									<option value="2" @if($item->cantidad == 2) selected @endif>
										2
									</option>
									<option value="3" @if($item->cantidad == 3) selected @endif>
										3
									</option>
									<option value="4" @if($item->cantidad == 4) selected @endif>
										4
									</option>
									<option value="5" @if($item->cantidad == 5) selected @endif>
										5
									</option>
								</select>
							</center>
					      </td>
					      <td class="text-center">
					      	<h2 style="display: inline;">$</h2>
					      	<h2 class="subtotal-ind" style="display: inline;">
					      		@if($item->plan == 1){{$product->precio*$item->cantidad}} @elseif($item->plan == 2){{$product->precio*2*$item->cantidad}} @elseif($item->plan == 3){{$product->precio*3*$item->cantidad}} @endif
					      	</h2>
					      </td>
					    </tr>
					    @endif
					    @endforeach
					    @endforeach
					  </tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-4 offset-md-8 col-sm-12 mt-4">
						<h4 class="subtotal1" style="display: inline;"></h4>
						<h3 class="subtotal2" style="color: #990000; display: inline; padding-left: 2%;"></h3>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-12 mt-4">
				<div class="container">
					<p class="font-weight-bold " style="color:blue;display: inline-block;">Compartir</p>
					<a href="facebook.com" class="ml-2"><span class="fab fa-facebook-f fa-2x d-inline-block"></span></a>
					<a href="twitter.com" class="ml-2"><span class="fab fa-twitter fa-2x d-inline-block"></span></a>
					<a href="instagram" class="ml-2"><span class="fab fa-instagram fa-2x d-inline-block"></span></a>
					<div class="row" style="background-color: #EEEEEE; border-radius: 5px;">
						<div class="col-12 mt-4 mb-4 ">
							<h4 class="subtotal1"></h4>
							<h3 class="subtotal2" style="color: #990000;"></h3>
							<button type="button" class="btn col-form-label-sm btn-carrito btn-comprar font-weight-bold mt-3" data-toggle="modal" data-target="#pagar">Proceder al Pago</button>
						</div>
					</div>

					<div class="row mt-4" style="background-color: #68BCFF;">
						<div>
							<a href="{{url('/categoria/Ofertas')}}">
								<img src="{{ asset('images/backend_images/products/medium/'.$produc_promo->image) }}" class="img-fluid">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>


@endsection


