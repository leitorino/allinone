@extends('layouts.publicLayout.public_design')
@section('content')
<div style="background-color: rgb(242,242,242);">
  <div id="carouselControls" class="carousel slide mb-4" data-ride="carousel">
    <div class="carousel-inner">
      <?php $cont=0; ?>
      @foreach($banners as $banner)
      @if($cont == 0)
        <div class="carousel-item active">
      @else
        <div class="carousel-item">
      @endif
        <img class="d-block w-100" src="{{ asset('images/frontend_images/banners/'.$banner->image) }}" alt="{{$banner->title}}">
      </div>
      <?php $cont=$cont+1; ?>
      @endforeach
    </div>
     <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Siguiente</span>
    </a>
  </div>
 

  <div class="mt-5">
    <div class="container" style="background-color: white;" >
      <div>
        <h4 style="display: inline-block;">Productos Más Vendidos</h4>
        <a href="{{ url('todos-productos') }}">Ver más</a>
      </div>
      <div class="mt-2 scroller">
        @foreach($toptenproducts as $product)
        
        <a href="{{ url('/producto/'.$product->id) }}" >
          <div class="box-prod-item ">
          <h2 class="jumbotron" style="font-size: 1.4rem;">{{$product->product_name}}</h2> 
          <img src="{{ asset('images/backend_images/products/medium/'.$product->image) }}" style="margin:1%;" class="img-fluid prod-item">
          </div>  
        </a>
        
        @endforeach
      </div>
    </div>
  </div>

<?php $cont = 0; ?>
@foreach($toptenproducts2 as $catprod)
@if($cont<3)
  <div class="mt-3">
    <div class="container" style="background-color: white;" >
      <div>
        <h4 style="display: inline-block;">Tendencias en {{$categoryname[$cont]}}</h4>
        <a href="{{ url('/categoria/'.$categoryurl[$cont]) }}">Ver más</a>
      </div>
      <div class="mt-2 scroller">
        @foreach($catprod as $product)
          <a href="{{ url('/producto/'.$product->id) }}" >
            <div class="box-prod-item">
              <h2 class="jumbotron"  style="font-size: 1.4rem;">{{$product->product_name}}</h2>
              <img src="{{ asset('images/backend_images/products/medium/'.$product->image) }}" style="margin:1%;" class="img-fluid prod-item">
            </div>
          </a>
        @endforeach
      </div>
    </div>
  </div>
      @if($cont==1)
          <div class="mt-3">
            <div class="container" style="background-color: white;" >
                <div class="row">
                    <div class="col-6 hover-regala">
                      <a @if(strpos( $categories_dropdown,'Diseño') != false) href="{{url('/todos-productos')}}"@else href="#" @endif title="">
                      <div class="row">
                        <div class="col-md-6 col-sm-12 text-center jumbotron title-links">
                          <h2>Regala un </h2>
                          <h1>Diseño</h1>
                        </div>
                        <div class="col-md-6 col-sm-12">
                          <img src="{{ asset('images/frontend_images/bolsa.jpg') }}" class="img-fluid">
                        </div>
                      </div>
                      </a>
                    </div>
                    <div class="col-6 hover-pop">
                      <a @if(strpos( $categories_dropdown,'POP') != false) href="{{url('/categoria/diseno-pop')}}"@else href="#" @endif title="">
                      <div class="row">
                        <div class="col-md-6 col-sm-12 text-center jumbotron title-links">
                          <h2>Material</h2>
                          <h1>Pop</h1>
                        </div>
                        <div class="col-md-6 col-sm-12" style="display: inline-block;">
                          <img src="{{ asset('images/frontend_images/taza.jpg') }}" class="img-fluid">
                        </div>
                      </div>
                      </a>
                    </div>
                </div>
            </div>
          </div>
      @endif
  @endif
  <?php $cont++; ?>
@endforeach

<section class="categories" type="categories">
    <div class="container mt-3">
        <h4>Categorías</h4>
        <div class="wrapper" style="background-color: white;">
            <div class="carousel-container">
                <div class="slick-initialized slick-slider">
                    <button class="prev-button arrow-disabled">
                    </button>
                    <div role="presentation" class="slick-list">
                        <div class="slick-track">
                          <?php $cont = 0; ?>
                          <div data-index="{{$cont/2}}" class="slick-slide" tabindex="-1" style="outline:none">
                              <div class="category-column">
                                <a class="category" href="{{ url('todos-productos') }}">
                                            <div class="icon">
                                                <img src="{{ asset('images/frontend_images/allcat.png') }}">
                                            </div>
                                            <p>Todos</p>
                                        </a>
                            <?php $cont = 1; ?>
                            @foreach($categories as $cat)
                            @if((($cont % 2)== 0))
                                <div data-index="{{$cont/2}}" class="slick-slide" tabindex="-1" style="outline:none">
                                    <div class="category-column">
                            @endif
                                        <a class="category" href="{{ url('/categoria/'.$cat->url) }}">
                                            <div class="icon">
                                                <img src="{{ asset('images/backend_images/category/small/'.$cat->image) }}">
                                            </div>
                                            <p>{{$cat->name}}</p>
                                        </a>
                            @if((($cont % 2)!= 0))
                                    </div>
                                </div>
                            @endif
                            
                            <?php $cont++; ?>
                            @endforeach
                        </div>
                    </div>
                    <button class="next-button ">
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
  @if(strpos( $categories_dropdown,'Ofertas') != false)
  <div class="row m-5">
    <div class="col-12 text-center">
        <a href="{{url('/categoria/Ofertas')}}"><img src="{{ asset('images/frontend_images/gif-ecomerce.gif') }}"></a>
    </div>
  </div>  
  @endif
</section>

<div class="volver-inicio">
  <center>
    <a href="#navbar">Inicio de Pagina</a>
  </center>
</div>
</div>



@endsection
