@extends('layouts.adminLayout.admin_design')
@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/add-product') }}" class="current">Agregar Imagenes del Producto</a> </div>
    <h1>Productos</h1>
  </div>
  @if(Session::has('flash_message_error'))
  <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong>{!! session('flash_message_error') !!} </strong>
  </div>          
  @endif
  @if(Session::has('flash_message_success'))
      <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{!! session('flash_message_success') !!} </strong>
      </div>          
  @endif
  <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Agregar Imagen</h5>
            </div>
             <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/add-images/'.$productDetails->id)}}" name="add_images" id="add_images" novalidate="novalidate">{{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Nombre del Producto</label>
                <label class="control-label"><b>{{ $productDetails->product_name }}</b></label>
              </div>
              <div class="control-group">
                <label class="control-label">Codigo del Producto</label>
                <label class="control-label"><b>{{ $productDetails->product_code }}</b></label>
              </div>
              <div class="control-group">
                <label class="control-label">Imagen del Producto</label>
                <div class="controls">
                  <input type="file" name="image[]" id="image" multiple="multiple">
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Agregar Imagen" class="btn btn-success">
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Ver Imagenes</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID de la imagen</th>
                  <th>ID del producto</th>
                  <th>Imagen</th>
                </tr>
              </thead>
              <tbody>
                @foreach($images as $image)
                  <tr class="gradeX ">
                  <td><p class="text-center">{{$image->id}}</p></td>
                  <td><p class="text-center">{{$image->product_id}}</p></td>
                  <td><div class="text-center"><img src="{{ asset('images/backend_images/products/small/'.$image->image) }}" style="width: 25%;"></div></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection