@extends('layouts.adminLayout.admin_design')
@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/add-product') }}" class="current">Agregar Producto</a> </div>
    <h1>Productos</h1>
  </div>
  @if(Session::has('flash_message_error'))
  <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong>{!! session('flash_message_error') !!} </strong>
  </div>          
  @endif
  @if(Session::has('flash_message_success'))
      <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{!! session('flash_message_success') !!} </strong>
      </div>          
  @endif
  <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Agregar Producto</h5>
            </div>
             <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/add-product')}}" name="add_product" id="add_product" novalidate="novalidate">{{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Categoria del Producto</label>
                <div class="controls">
                  <select name="category_id" style="width: 25%;">
                    <?php echo $categories_dropdown; ?>
                    <option selected disabled>Categorias</option>
                  </select>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Nombre del Producto</label>
                <div class="controls">
                  <input type="text" name="product_name" id="product_name">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Codigo del Producto</label>
                <div class="controls">
                  <input type="text" name="product_code" id="product_code">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Descripcion del Producto</label>
                <div class="controls">
                  <textarea name="desc" id="desc"></textarea>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Descripcion Detallada del Producto</label>
                <div class="controls">
                  <textarea name="descdet" id="descdet"></textarea>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Precio del Producto</label>
                <div class="controls">
                  <input type="text" name="precio" id="precio">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Imagen del Producto</label>
                <div class="controls">
                  <input type="file" name="product_image" id="product_image">
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Agregar Producto" class="btn btn-success">
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection