@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/add-category') }}" class="current">Editar Producto</a> </div>
    <h1>Productos</h1>
  </div>
  <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Editar Categoria</h5>
            </div>
             <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/edit-product/'.$productDetails->id)}}" name="edit_product" id="edit_product" novalidate="novalidate">{{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Categoria del Producto</label>
                <div class="controls">
                  <select name="category_id" style="width: 25%;">
                    <?php echo $categories_dropdown; ?>
                  </select>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Nombre del Producto</label>
                <div class="controls">
                  <input type="text" name="product_name" id="product_name" value="{{$productDetails->product_name}}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Codigo del Producto</label>
                <div class="controls">
                  <input type="text" name="product_code" id="product_code" value="{{$productDetails->product_code}}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Descripcion del Producto</label>
                <div class="controls">
                  <textarea name="desc" id="desc" value="{{$productDetails->description}}"></textarea>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Descripcion Detallada del Producto</label>
                <div class="controls">
                  <textarea name="descdet" id="descdet" value="{{$productDetails->descriptiondetallado}}"></textarea>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Precio del Producto</label>
                <div class="controls">
                  <input type="text" name="precio" id="precio" value="{{$productDetails->precio}}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Imagen del Producto</label>
                <div class="controls">
                  <input type="file" name="product_image" id="product_image">
                  <input type="hidden" name="current_image" value="{{ $productDetails->image }}">
                  <img src="{{ asset('images/backend_images/products/small/'.$productDetails->image) }}" style="width: 5%;"> | <a href="{{ url('/delete-product-image/'.$productDetails->id) }}">Borrar</a>
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Editar Categoria" class="btn btn-success">
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection