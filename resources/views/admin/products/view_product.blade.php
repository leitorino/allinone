@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/view-product') }}" class="current">Ver Producto</a> </div>
    <h1>Productos</h1>
  </div>
  	@if(Session::has('flash_message_success'))
	    <div class="alert alert-success alert-block">
	        <button type="button" class="close" data-dismiss="alert">x</button>
	        <strong>{!! session('flash_message_success') !!} </strong>
	    </div>          
	  @endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Ver Categorias</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Categoria</th>
                  <th>Nombre</th>
                  <th>Codigo</th>
                  <th>Descripcion</th>
                  <th>Ventas</th>
                  <th>Precio</th>
                  <th>Imagen</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($products as $product)
                <tr class="gradeX ">
		            <td><p class="text-center">{{$product->id}}</p></td>
		            <td><p class="text-center">{{$product->category_name}}</p></td>
                <td><p class="text-center">{{$product->product_name}}</p></td>
		            <td><p class="text-center">{{$product->product_code}}</p></td>
		            <td><p class="text-center">{{$product->description}}</p></td>
                <td><p class="text-center">{{$product->compras}}</p></td>
                <td><p class="text-center">{{$product->precio}}</p></td>
                <td><div class="text-center"><img src="{{ asset('images/backend_images/products/small/'.$product->image) }}" style="width: 25%;"></div></td>
		            <td class="center">
	                  	<center>
                        <a href="#myModal{{$product->id}}" data-toggle="modal" class="btn btn-success btn-mini">Ver</a>
  		                  <a href="{{url('/admin/edit-product/'.$product->id)}}" class="btn btn-primary btn-mini">Editar</a>
                        <a href="{{url('/admin/add-images/'.$product->id)}}" class="btn btn btn-mini">Imagenes</a>
  		              	  <a rel="{{ $product->id }}" rel1="delete-product" id="delProd" href="#" {{-- href="{{url('/admin/delete-product/'.$product->id)}}" --}} class="btn btn-danger btn-mini delProd">Borrar</a>
	              		</center>
              		</td>
                </tr>
                <div id="myModal{{$product->id}}" class="modal hide">
                  <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="buttom">x</button>
                    <h3>{{$product->product_name}}</h3>
                  </div>
                  <div class="modal-body">
                    <div class="text-center"><img src="{{ asset('images/backend_images/products/big/'.$product->image) }}"></div>
                  </div>
                </div>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection