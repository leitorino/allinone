@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Ordenes</a> <a href="#" class="current">Ver Oredenes</a> </div>
    <h1>Ordenes</h1>
    @if(Session::has('flash_message_error'))
      <div class="alert alert-error alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{!! session('flash_message_error') !!}</strong>
      </div>
    @endif   
    @if(Session::has('flash_message_success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{!! session('flash_message_success') !!}</strong>
        </div>
    @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Ordenes</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID de la orden</th>
                  <th>Fecha de la orden</th>
                  <th>Id del comprador</th>
                  <th>Customer Email</th>
                  <th>Ordered Products</th>
                  <th>Order Amount</th>
                  <th>Order Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($orders as $order)
                <tr class="gradeX">
                  <td class="center">{{ $order->id }}</td>
                  <td class="center">{{ $order->created_at }}</td>
                  <td class="center">{{ $order->user_id }}</td>
                  <td class="center">{{ $order->email }}</td>
                  <td class="center">
                    @foreach($order->orders as $pro)
                    {{ $pro->id }}
                    ({{ $pro->cantidad }})
                    <br>
                    @endforeach
                  </td>
                  <td class="center">{{ $order->total }}</td>
                  <td class="center">{{ $order->status }}</td>
                  <td class="center">
                    <a target="_blank" href="{{ url('admin/view-order/'.$order->id)}}" class="btn btn-success btn-mini">Ver detalles de la orden</a> 
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="text-right">
           <a href="{{url('/admin/delete-orders')}}"><button class="btn btn-primary">Borrar Pedidos En Espera de Pago</button></a> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection