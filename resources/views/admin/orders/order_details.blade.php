@extends('layouts.adminLayout.admin_design')
@section('content')

<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Ordenes</a> </div>
    <h1>Orden #{{ $orderDetails->id }}</h1>
    @if(Session::has('flash_message_success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{!! session('flash_message_success') !!}</strong>
        </div>
    @endif
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title">
            <h5>Orden Detallada</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-striped table-bordered">
              <tbody>
                <tr>
                  <td class="taskDesc">Order Date</td>
                  <td class="taskStatus">{{ $orderDetails->created_at }}</td>
                </tr>
                <tr>
                  <td class="taskDesc">Order Total</td>
                  <td class="taskStatus">$ {{ $orderDetails->total }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title">
            <h5>Detalles del Cliente</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-striped table-bordered">
              <tbody>
                <tr>
                  <td class="taskDesc">Nombre del Cliente</td>
                  <td class="taskStatus">{{ $userDetails->name }}</td>
                </tr>
                <tr>
                  <td class="taskDesc">Email del Cliente</td>
                  <td class="taskStatus">{{ $userDetails->email }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="accordion" id="collapse-group">
          <div class="accordion-group widget-box">
            <div class="accordion-heading">
              <div class="widget-title"> 
                <h5>Actualizar Estado de la Orden</h5>
              </div>
            </div>
            <div class="collapse in accordion-body" id="collapseGOne">
              <div class="widget-content"> 
                <form action="{{ url('admin/update-order-status') }}" method="post">{{ csrf_field() }}
                  <input type="hidden" name="order_id" value="{{ $orderDetails->id }}">
                  <table width="100%">
                    <tr>
                      <td>
                        <select name="order_status" id="order_status" class="control-label" required="">
                          <option value="Nuevo" @if($orderDetails->status == "Nuevo") selected @endif>Nuevo</option>
                          <option value="Pendiente" @if($orderDetails->status == "Pediente") selected @endif>Pendiente</option>
                          <option value="Cancelado" @if($orderDetails->status == "Cancelado") selected @endif>Cancelado</option>
                          <option value="Realizando" @if($orderDetails->status == "In Realizando") selected @endif>Realizando</option>
                          <option value="Modificando" @if($orderDetails->status == "Modificando") selected @endif>Modificando</option>
                          <option value="Finalizado" @if($orderDetails->status == "Finalizado") selected @endif>Finalizado</option>
                          <option value="Pagado" @if($orderDetails->status == "Pagado") selected @endif>Pagado</option>
                        </select>
                      </td>
                      <td>
                        <input type="submit" value="Actualizar Estado">
                      </td>
                    </tr>
                  </table>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row-fluid">
      <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
              <tr>
                  <th>Nombre del Producto</th>
                  <th>Precio Individual</th>
                  <th>Cantidad de Productos</th>
              </tr>
          </thead>
          <tbody>
            @foreach($orderDetails->orders as $pro)
            <?php 
            foreach ($products as $prod) {
              if($prod->id == $pro->product_id){
                $name = $prod->product_name;
                $precio =  $pro->plan * $prod->precio;
              }
            }
            ?>
              <tr>
                  <td>{{ $name }}</td>
                  <td>{{ $precio }}</td>
                  <td>{{ $pro->cantidad }}</td>
              </tr>
              @endforeach
          </tbody>
      </table>
    </div>
  </div>
</div>
<!--main-container-part-->

@endsection