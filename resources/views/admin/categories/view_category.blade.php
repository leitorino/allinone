@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/view-category') }}" class="current">Ver Category</a> </div>
    <h1>Categorias</h1>
  </div>
  	@if(Session::has('flash_message_success'))
	    <div class="alert alert-success alert-block">
	        <button type="button" class="close" data-dismiss="alert">x</button>
	        <strong>{!! session('flash_message_success') !!} </strong>
	    </div>          
	@endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Ver Categorias</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Nivel</th>
                  <th>Descripcion</th>
                  <th>Url</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($categories as $category)
                <tr class="gradeX">
		            <td><p class="text-center">{{$category->id}}</p></td>
		            <td><p class="text-center">{{$category->name}}</p></td>
                <td><p class="text-center">{{$category->parent_id}}</p></td>
		            <td><p class="text-center">{{$category->description}}</p></td>
		            <td><p class="text-center">{{$category->url}}</p></td>
		            <td class="center">
	                  	<center>
		                  <a href="{{url('/admin/edit-category/'.$category->id)}}" class="btn btn-primary btn-mini">Editar</a>
		              	  <a id="delCat" rel="{{ $category->id }}" rel1="delete-category" href="#{{-- {{url('/admin/delete-category/'.$category->id)}} --}}" class="btn btn-danger btn-mini delCat">Borrar</a>
	              		</center>
              		</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection