@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/add-publicidad') }}" class="current">Editar Publicidad</a> </div>
    <h1>Publicidad</h1>
  </div>
  <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Editar Publicidad</h5>
            </div>
             <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/edit-publicidad/'.$publicidadDetails->id)}}" name="edit_publicidad" id="edit_publicidad" novalidate="novalidate">{{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Habilitar</label>
                <div class="controls">
                  @if($publicidadDetails->status == 0)

                    <input type="checkbox" name="status" id="status">

                  @else

                    <input type="checkbox" name="status" id="status" checked>

                  @endif
                  
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Imagen del Publicidad</label>
                <div class="controls">
                  <input type="file" name="publicidad_image" id="publicidad_image">
                  <input type="hidden" name="current_image" value="{{ $publicidadDetails->image }}">
                  <img src="{{ asset('images/frontend_images/publicidads/'.$publicidadDetails->image) }}" style="width: 5%;">
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Editar Publicidad" class="btn btn-success">
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection