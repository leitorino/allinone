@extends('layouts.adminLayout.admin_design')
@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/add-publicidad') }}" class="current">Agregar Publicidad</a> </div>
    <h1>Publicidad</h1>
  </div>
  @if(Session::has('flash_message_error'))
  <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong>{!! session('flash_message_error') !!} </strong>
  </div>          
  @endif
  @if(Session::has('flash_message_success'))
      <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{!! session('flash_message_success') !!} </strong>
      </div>          
  @endif
  <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Agregar Publicidad</h5>
            </div>
             <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/add-publicidad')}}" name="add_publicidad" id="add_publicidad" novalidate="novalidate">{{ csrf_field() }}
            	<div class="control-group">
				<label class="control-label">Publicidad</label>
					<div class="controls">
						<input type="file" name="publicidad_image" id="publicidad_image">
					</div>
				</div>

              <div class="control-group">
                <label class="control-label">Habilitar publicidad</label>
                <div class="controls">
                    <input type="checkbox" name="status" id="status" checked>    
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Agregar Publicidad" class="btn btn-success">
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection