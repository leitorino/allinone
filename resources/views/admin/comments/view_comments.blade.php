@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/view-category') }}" class="current">Ver Comentarios</a> </div>
    <h1>Comentarios</h1>
  </div>
  	@if(Session::has('flash_message_success'))
	    <div class="alert alert-success alert-block">
	        <button type="button" class="close" data-dismiss="alert">x</button>
	        <strong>{!! session('flash_message_success') !!} </strong>
	    </div>          
	@endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Ver Comentarios</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>ID del Usuario</th>
                  <th>ID del Producto</th>
                  <th>Comentario</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($comments as $comment)
                <tr class="gradeX">
		            <td style="width: 5%;"><p class="text-center">{{$comment->id}}</p></td>
		            <td style="width: 10%"><p class="text-center">{{$comment->user_id}}</p></td>
                <td style="width: 10%"><p class="text-center">{{$comment->product_id}}</p></td>
		            <td><p>{{$comment->comment}}</p></td>
		            <td class="center">
	                  	<center>
		              	  <a id="delCat" rel="{{ $comment->id }}" rel1="delete-comment" href=" {{url('/admin/delete-comment/'.$comment->id)}}" class="btn btn-danger btn-mini delCat">Borrar</a>
	              		</center>
              		</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection