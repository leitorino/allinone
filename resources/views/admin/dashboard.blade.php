@extends('layouts.adminLayout.admin_design')
@section('content')

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->  

<!--Chart-box-->    
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>Site Analytics</h5>
        </div>
        <div class="widget-content" >
          <div class="row-fluid">
            <div class="span9">
              <div class="chart"></div>
            </div>
            <div class="span3">
              <ul class="site-stats">
                <li class="bg_lh"><strong>{{$users_cont}}</strong> <small>Total Users</small></li>
                <li class="bg_lh"><strong>{{$newusers_cont}}</strong> <small>Nuevos Users </small></li>
                <li class="bg_lh"><strong>{{$orders_cont}}</strong> <small>Total de Compras</small></li>
                <li class="bg_lh"> <strong>{{$orderspend_cont}}</strong> <small>Ordenes Pendientes</small></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
<!--End-Chart-box--> 
    <hr/>
    <div class="row-fluid">
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
            <h5>Ultimos Comentarios</h5>
          </div>
          <div class="widget-content nopadding collapse in" id="collapseG2">
            <ul class="recent-posts">
              @foreach($comments as $comment)
              <li>
                <div class="user-thumb"> <img width="40" height="40" alt="User" src="{{ asset('images/backend_images/demo/av4.jpg') }}"> </div>
                <div class="article-post"> <span class="user-info"> Por: {{$comment->names->username}} / Date: {{$comment->created_at}} </span>
                  <p>{{$comment->comment}}</p>
                </div>
              </li>
              @endforeach
              <li>
                <a href="{{url('/admin/view-comments')}}"></a><button class="btn btn-warning btn-mini">Ver todos</button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--end-main-container-part-->

@endsection