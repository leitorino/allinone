@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/add-category') }}" class="current">Editar Usuario</a> </div>
    <h1>Usuarios</h1>
  </div>
  <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Editar Usuario</h5>
            </div>
             <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/edit-user/'.$userdetail->id)}}" name="edit_user" id="edit_product" novalidate="novalidate">{{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Nombre</label>
                <div class="controls">
                  <input type="text" name="user_name" id="user_name" value="{{$userdetail->name}}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Habilitar como Administrador</label>
                <div class="controls">
                  @if($userdetail->admin == 0)

                    <input type="checkbox" name="status" id="status">

                  @else

                    <input type="checkbox" name="status" id="status" checked>

                  @endif
                  
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Editar Usuario" class="btn btn-success">
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection