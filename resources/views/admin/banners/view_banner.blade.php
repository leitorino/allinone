@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/view-category') }}" class="current">Ver Banners</a> </div>
    <h1>Banners</h1>
  </div>
  	@if(Session::has('flash_message_success'))
	    <div class="alert alert-success alert-block">
	        <button type="button" class="close" data-dismiss="alert">x</button>
	        <strong>{!! session('flash_message_success') !!} </strong>
	    </div>          
	@endif
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Ver Categorias</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Titulo</th>
                  <th>Link</th>
                  <th>Status</th>
                  <th>Imagen</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
              	@foreach($banners as $banner)
                <tr class="gradeX">
		            <td style="width: 5%;"><p class="text-center">{{$banner->id}}</p></td>
		            <td style="width: 20%"><p class="text-center">{{$banner->title}}</p></td>
                <td><p class="text-center">{{$banner->link}}</p></td>
		            <td><p class="text-center">{{$banner->status}}</p></td>
		            <td style="padding-left: 0px;padding-right: 0px;border-left-width: 0px;border-top-width: 0px;width: 20%;"><div class="text-center" ><img src="{{ asset('images/frontend_images/banners/'.$banner->image) }}" width="25%"></div></td>
		            <td class="center">
	                  	<center>
		                  <a href="{{url('/admin/edit-banner/'.$banner->id)}}" class="btn btn-primary btn-mini">Editar</a>
		              	  <a id="delCat" rel="{{ $banner->id }}" rel1="delete-banner" href="#{{-- {{url('/admin/delete-banner/'.$banner->id)}} --}}" class="btn btn-danger btn-mini delCat">Borrar</a>
	              		</center>
              		</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection