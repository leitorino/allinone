@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/add-banner') }}" class="current">Editar Banner</a> </div>
    <h1>Banners</h1>
  </div>
  <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Editar Banner</h5>
            </div>
             <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/edit-banner/'.$bannerDetails->id)}}" name="edit_banner" id="edit_banner" novalidate="novalidate">{{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Titulo del Banner</label>
                <div class="controls">
                  <input type="text" name="banner_title" id="banner_title" value="{{$bannerDetails->title}}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">link del banner</label>
                <div class="controls">
                  <input type="text" name="banner_link" id="banner_link" value="{{$bannerDetails->banner_code}}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Habilitar</label>
                <div class="controls">
                  @if($bannerDetails->status == 0)

                    <input type="checkbox" name="status" id="status">

                  @else

                    <input type="checkbox" name="status" id="status" checked>

                  @endif
                  
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Imagen del banner</label>
                <div class="controls">
                  <input type="file" name="banner_image" id="banner_image">
                  <input type="hiden" name="current_image" value="{{ $bannerDetails->image }}">
                  <img src="{{ asset('images/frontend_images/banners/'.$bannerDetails->image) }}" style="width: 5%;">
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Editar Banner" class="btn btn-success">
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection