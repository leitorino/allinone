@extends('layouts.adminLayout.admin_design')
@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/admin/dashboard') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ url('/admin/add-product') }}" class="current">Agregar Producto</a> </div>
    <h1>Productos</h1>
  </div>
  @if(Session::has('flash_message_error'))
  <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">x</button>
      <strong>{!! session('flash_message_error') !!} </strong>
  </div>          
  @endif
  @if(Session::has('flash_message_success'))
      <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{!! session('flash_message_success') !!} </strong>
      </div>          
  @endif
  <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Agregar Producto</h5>
            </div>
             <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/add-banner')}}" name="add_banner" id="add_banner" novalidate="novalidate">{{ csrf_field() }}
            	<div class="control-group">
				<label class="control-label">Banner</label>
					<div class="controls">
						<input type="file" name="banner_image" id="banner_image">
					</div>
				</div>
              <div class="control-group">
                <label class="control-label">Titulo del Banner</label>
                <div class="controls">
                  <input type="text" name="banner_title" id="banner_title">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Link del Banner</label>
                <div class="controls">
                  <input type="text" name="banner_link" id="banner_link">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Habilitar Banner</label>
                <div class="controls">
                    <input type="checkbox" name="status" id="status" checked>    
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Agregar Banner" class="btn btn-success">
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection