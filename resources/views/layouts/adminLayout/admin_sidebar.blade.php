

<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li><a href="{{url('/admin/dashboard')}}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Banners</span> <span class="label label-important">2</span></a>
      <ul>
        <li><a href="{{url('/admin/add-banner')}}">Agregar Banner</a></li>
        <li><a href="{{url('/admin/view-banners')}}">Ver Banners</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Categorias</span> <span class="label label-important">2</span></a>
      <ul>
        <li><a href="{{url('/admin/add-category')}}">Agregar Categoria</a></li>
        <li><a href="{{url('/admin/view-category')}}">Ver Categorias</a></li>
      </ul>
    </li>
    <li><a href="{{url('/admin/view-comments')}}"><i class="icon icon-pencil"></i> <span>Comentarios</span></a></li>
    <li><a href="{{url('/admin/view-orders')}}"><i class="icon icon-pencil"></i> <span>Ordenes</span></a></li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Productos</span> <span class="label label-important">2</span></a>
      <ul>
        <li><a href="{{url('/admin/add-product')}}">Agregar Producto</a></li>
        <li><a href="{{url('/admin/view-product')}}">Ver Productos</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Publicidad</span> <span class="label label-important">2</span></a>
      <ul>
        <li><a href="{{url('/admin/add-publicidad')}}">Agregar Publicidad</a></li>
        <li><a href="{{url('/admin/view-publicidad')}}">Ver Anuncios Publicitarios</a></li>
      </ul>
    </li>
    <li><a href="{{url('/admin/view-users')}}"><i class="icon icon-pencil"></i> <span>Usuarios</span></a></li>
  </ul>
</div>
<!--sidebar-menu-->