<header class="nav-opt-sprite">
  <div class="row">
    <div class="col-12">
        <div id="navbar" cel_widget_id="Navigation-desktop-navbar" role="navigation" class="nav-sprite-v1 celwidget nav-bluebeacon nav-packard-glow">  
        <div id="nav-belt">  
          <div class="nav-left">
            <div id="nav-logo">
                <a href="{{url('/')}}" class="nav-logo-link" aria-label="AllInOne" tabindex="6">
                  <img class="img-fluid" src="{{ asset('images/backend_images/logo.png') }}" alt="Logo" width="100%;" />
                </a>
            </div>
          </div>
          <div class="nav-right">     
            <div id="nav-swmslot">
                
            </div>
          </div>
          <div class="nav-fill">
            <div id="nav-search">
              <div id="nav-bar-left"></div>
              <form class="nav-searchbar" action="{{ url('/search-products') }}" method="post" role="search">{{ csrf_field() }} 
                <div class="nav-left">
                  <div id="nav-search-dropdown-card"> 
                      <div class="nav-search-scope nav-sprite">
                        <div class="nav-search-facade" data-value="search-alias=aps">
                          <span class="nav-search-label">Todos</span>
                          <i class="nav-icon"></i>
                        </div>
                        <span id="searchDropdownDescription" style="display:none">Selecciona el departamento que quieras buscar</span>
                        <select aria-describedby="searchDropdownDescription" class="nav-search-dropdown searchSelect" data-nav-digest="Xa0GQ+pPQ/tdsV+GmRWeXB8PUD0=" data-nav-selected="0" id="searchDropdownBox" name="url" style="display: block;" tabindex="18" title="Search in">
                            <option value="Todos">Todos los Departamentos</option>
                            <?php echo $categories_dropdown; ?>
                        </select>
                      </div>
                  </div>
                </div>
                <div class="nav-right">
                  <div class="nav-search-submit nav-sprite">
                    <span class="fas fa-search" style="font-size: 1.5rem; padding-left: 20%; padding-top: 15%;  color: white;"></span>
                    <input type="submit" class="nav-input" value="Go" tabindex="20">
                  </div>
                </div>
                <div class="nav-fill">
                  <div class="nav-search-field ">
                    <!-- DO NOT REMOVE: the text in the label are for screen reader, and it is not visible in the web page -->
                    <input type="text" name="product" autocomplete="off" placeholder="" class="nav-input" tabindex="19">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div id="nav-main" class="nav-sprite"> 
          
          <div class="nav-left">

          </div>

          <div class="nav-right">
            {{-- <script type="text/javascript">window.navmet.tmp=+new Date();</script> --}}
            <div id="nav-tools">  
              
            <a href="/gp/customer-preferences/select-language/ref=topnav_lang?preferencesReturnUrl=%2F" id="icp-nav-flyout" class="nav-a nav-a-2 icp-link-style-2">
                <span class="icp-nav-link-inner">
                  <span class="nav-line-1">
                    <span class="icp-nav-globe-img-2"></span>
                    <span class="icp-nav-language">ES</span>
                  </span>
                  <span class="nav-line-2">&nbsp;
                    <span class="nav-icon nav-arrow"></span>
                  </span>
                </span>
                <span class="icp-nav-link-border"></span>
            </a>

              
            @guest
            <a href="{{ route('login') }}" class="nav-a nav-a-2" data-nav-ref="nav_ya_signin" data-nav-role="signin" data-ux-jq-mouseenter="true" id="nav-link-accountList" tabindex="61">
              <span class="nav-line-1">Hola.</span>
              <span class="nav-line-2">Inicia Sesion</span>
            </a>
            @else
            <span class="nav-a nav-a-2" data-nav-ref="nav_ya_signin" data-nav-role="signin" data-ux-jq-mouseenter="true" id="nav-link-accountList" tabindex="61">
              <span class="nav-line-1">Hola. {{ Auth::user()->username }}</span>
              <span class="nav-line-2"><a style="color: white"  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar Sesion</a></span>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </span>

               <!--  <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Cerrar Sesion') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form> -->
            @endguest

            @guest
            <a href="{{ route('login') }}" class="nav-a nav-a-2 nav-single-row-link" id="nav-orders" tabindex="63">
            @else
            <a href="{{url('/pedidos')}}" class="nav-a nav-a-2 nav-single-row-link" id="nav-orders" tabindex="63">
              <span class="nav-line-1"></span>
              <span class="nav-line-2">Pedidos</span>
            </a>
            @endguest
            
              
            <a href="{{url('/carrito')}}" aria-label="0 items in cart" class="nav-a nav-a-2" id="nav-cart" tabindex="65">
                <span aria-hidden="true" class="nav-line-1"></span>
                <span aria-hidden="true" class="nav-line-2">Carrito<span class="nav-icon nav-arrow"></span>
                </span>
                <span class="fas fa-shopping-cart nav-cart-icon nav-sprite" style="font-size: 1.5rem; color: white;"></span>
                <!-- <span id="nav-cart-count" aria-hidden="true" class="nav-cart-count nav-cart-0">{{1}}</span> -->
            </a>

            </div>
            {{-- <script type="text/javascript">window.navmet.push({key:'Tools',end:+new Date(),begin:window.navmet.tmp});</script> --}}
            </div>
                <div class="nav-fill">
                        <div id="nav-xshop-container" class="">
                            <div id="nav-xshop">
                                <div id="nav-your-amazon" class="nav-a">&nbsp;</div>

                                <a href="{{url('/categorias')}}" class="nav-a" tabindex="48">Departamentos<i class="nav-icon"></i></a>

                                @guest
                                @else
                                <a href="{{url('/favoritos')}}" class="nav-a" tabindex="49">Favoritos<i class="nav-icon"></i></a>
                                </a>
                                @endguest
                                
                                @if(strpos( $categories_dropdown,'Ofertas') != false)
                                <a href="{{url('/categoria/Ofertas')}}" class="nav-a" tabindex="50">Ofertas</a>
                                @endif

                                <a href="{{url('/ayuda')}}" class="nav-a">Ayuda</a>
                            </div> 
                        </div>        
                    </div>
                </div>
            </div>
          </div>
        </div>
    </header>