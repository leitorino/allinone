<div class="body-footer-1">
	<div class="mb-5 container" style="padding-top: 10%">
		<div class="row">
			<div class="col-12 p-5 foot-icons">
				<center>
						<div class="col-1 d-inline"><a href="https://es-la.facebook.com/SaturnTeam/" ><img src="{{ asset('images/frontend_images/facebook.png') }}" class="img-fluid"></a></div>
						<div class="col-1 d-inline"><a href="https://www.instagram.com/saturn_team/" ><img src="{{ asset('images/frontend_images/instagram.png') }}" class="img-fluid"></a></div>
						<div class="col-1 d-inline"><a href="https://www.linkedin.com/company/saturnteam/about/" ><img src="{{ asset('images/frontend_images/linkedin.png') }}" class="img-fluid"></a></div>
						<div class="col-1 d-inline"><a href="https://twitter.com/saturnteams?lang=es" ><img src="{{ asset('images/frontend_images/twitter.png') }}" class="img-fluid"></a></div>
						<div class="col-1 d-inline"><a href="https://www.youtube.com/channel/UCuGDQVfGS_XYUHezQorEDQg" ><img src="{{ asset('images/frontend_images/youtube.png') }}" class="img-fluid"></a></div>
				</center>
			</div>
			<div class="col-md-3 col-sm-12 offset-md-2 mt-2">
				<h3 style="color: white;">Conócenos</h3>
				<div><a href="saturnteam.com">Acerca de Saturn Team</a></div>
				<div><a href="https://saturnteam.com/blog/">Blog</a></div>
				<div><a href="https://saturnteam.com/estudio-de-diseno/">Quienes Somos</a></div>
			</div>
			<div class="col-md-3 col-sm-12 mt-2">
				<h3 style="color: white;">Podemos ayudarte</h3>
				<div><a href="#">Tu cuenta</a></div>
				<div><a href="{{url('pedidos')}}">Tus pedidos</a></div>
				<div><a href="#">Tarifas y politicas</a></div>
				<div><a href="{{url('ayuda')}}">Ayuda</a></div>
			</div>
			<div class="col-md-3 col-sm-12 mt-2">
				<div><i class="fas fa-envelope mr-2"></i>Correo: <br> info@Saturnteam.com</div>
				<div><i class="fas fa-phone mr-2"></i>WhatsApp: 
					<li>+61434741270</li>
					<li>+4145804267</li>
				</div>
				<div><i class="fas fa-map-marker-alt mr-2"></i>Dirección: <br> Av Alejos Zuluado, Calle 146 San Ignicio, Quinta lita, Trigal Norte, Valencia, Carabobo</div>
			</div>
		</div>
	</div>
	<br class="m-5">
	<div class="m-5">
		<center>
			<img src="{{ asset('images/backend_images/logo.png') }}">
		</center>
	</div>
</div>
<div class="body-footer-2">
	<center>
		<b>Condiciones de uso Aviso de privacidad Anuncios basados en intereses</b> 2018, SaturnTeam.com 
	</center>
</div>
