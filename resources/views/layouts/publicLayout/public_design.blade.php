<!DOCTYPE html>
<html lang="es">
<head>
<title>All in One</title>
<!-- Programado Por Alcides Leon -->
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="{{ asset('css/frontend/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('css/frontend/amazon.css') }}" />
<link rel="stylesheet" href="{{ asset('css/frontend/core-style.css') }}" />
<link rel="stylesheet" href="{{ asset('css/frontend/classy-nav.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/frontend/easyzoom.css') }}" />
<link rel="stylesheet" href="{{ asset('css/frontend/owl.carousel.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/frontend/owl.theme.default.min.css') }}" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.4/dist/sweetalert2.min.css" integrity="sha256-uMEgdSzF6KzpJADQ5oAz2/+Pic69IkX0lyubjbNelv0=" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>

@include('layouts.publicLayout.public_header')

@yield('content')

@include('layouts.publicLayout.public_footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.4/dist/sweetalert2.min.js" integrity="sha256-/lJEdgB2gw+FlHCert3RROYUb000qa4W71w0+/ra5Wk=" crossorigin="anonymous"></script>
<!-- <script src="{{ asset('js/backend/jquery.min.js') }}"></script>  --> 
<!-- <script src="{{ asset('js/backend/matrix.js') }}"></script>  -->
<script src="{{ asset('js/frontend/jquery.js') }}"></script>
<script src="{{ asset('js/backend/jquery.ui.custom.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="{{ asset('js/backend/bootstrap.min.js') }}"></script> 
<script src="{{ asset('js/frontend/owl.carousel.js') }}"></script>
<script src="{{ asset('js/frontend/amazon.js') }}"></script> 
<script src="{{ asset('js/frontend/front.js') }}"></script>
<script src="{{ asset('js/frontend/easyzoom.js') }}"></script>


</body>
</html>