<!DOCTYPE html>
<html lang="es">
<head>
<title>All in One</title>
<!-- Programado Por Alcides Leon -->
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!-- <link rel="stylesheet" href="{{ asset('css/backend/jquery.gritter.css') }}" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'> -->
<link rel="stylesheet" href="{{ asset('css/frontend/amazon.css') }}" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.4/dist/sweetalert2.min.css" integrity="sha256-uMEgdSzF6KzpJADQ5oAz2/+Pic69IkX0lyubjbNelv0=" crossorigin="anonymous">
</head>
<body style="background-color: rgb(242,242,242);">

@yield('content')

@include('layouts.loginLayout.login_footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.4/dist/sweetalert2.min.js" integrity="sha256-/lJEdgB2gw+FlHCert3RROYUb000qa4W71w0+/ra5Wk=" crossorigin="anonymous"></script>
<script src="{{ asset('js/backend/jquery.min.js') }}"></script> 
<script src="{{ asset('js/backend/jquery.ui.custom.js') }}"></script> 
<script src="{{ asset('js/backend/bootstrap.min.js') }}"></script> 
<!-- <script src="{{ asset('js/backend/matrix.js') }}"></script>  -->
<script src="{{ asset('js/frontend/jquery.js') }}"></script>

</body>
</html>