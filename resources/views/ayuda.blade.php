@extends('layouts.publicLayout.public_design')
@section('content')

<div class="container">
    <div id="accordion">
      <!-- 1 -->
      <div class="card">
        <div class="card-header card-saturn" id="headingOne">
          <h5 class="mb-0">
            <button class="text-uppercase btn btn-link btn-saturn btn-block" data-toggle="collapse" data-target="#collapse-saturn-1" aria-expanded="true" aria-controls="collapse-saturn-1">
                1.- ¿Qué tipo de método de pagos tienen?
            </button>
          </h5>
        </div>
        <div id="collapse-saturn-1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
          <div class="card-body text-justify">
            Para realizar tus pagos, te ofrecemos una gran variedad de opciones. Si requieres mayor información, comunícate con nosotros por medio de nuestro correo electrónico: info@Saturnteam.com.
          </div>
        </div>
      </div>
      <!-- 2 -->
      <div class="card">
        <div class="card-header card-saturn" id="headingTwo">
          <h5 class="mb-0">
            <button class="text-uppercase btn btn-link btn-saturn collapsed btn-block" data-toggle="collapse" data-target="#collapse-saturn-2" aria-expanded="false" aria-controls="collapse-saturn-2">
                2.- ¿Qué tipo de servicios poseen adicionalmente?
            </button>
          </h5>
        </div>
        <div id="collapse-saturn-2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
          <div class="card-body text-justify">
            Saturnteam te ofrece una amplia gama de servicios, diseñados para satisfacer todas tus necesidades y las de tu proyecto. Tenemos:
            <li>REDES SOCIALES</li>
            <li>PAGINAS WEBS</li>
            <li>ILUSTRACIONES</li>
            <li>LOGOS</li>
            <li>PAPELERIA</li>
            <li>SEM</li>
            <li>SEO</li>
            <li>DISEÑO GENERAL</li>
          </div>
        </div>
      </div>
      <!-- 3 -->
      <div class="card">
        <div class="card-header card-saturn" id="headingThree">
          <h5 class="mb-0">
            <button class="text-uppercase btn btn-link btn-saturn collapsed btn-block" data-toggle="collapse" data-target="#collapse-saturn-3" aria-expanded="false" aria-controls="collapse-saturn-3">
                3.- ¿Cómo es el procedimiento para la compra?
            </button>
          </h5>
        </div>
        <div id="collapse-saturn-3" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
          <div class="card-body text-justify">
            Comprar con nosotros es muy sencillo. Primero, crea tu cuenta; luego, ve a la sección del producto que necesitas, selecciona el producto, verifica tu carrito de compra, has click en “procesar pago”, suministra a tu plataforma de pago los datos necesarios ¡y listo! Tu compra se ha procesado.
          </div>
        </div>
      </div>
      <!-- 4 -->
      <div class="card">
        <div class="card-header card-saturn" id="headingThree">
          <h5 class="mb-0">
            <button class="text-uppercase btn btn-link btn-saturn collapsed btn-block" data-toggle="collapse" data-target="#collapse-saturn-4" aria-expanded="false" aria-controls="collapse-saturn-4">
                4.- ¿Cuáles son los planes y costos?
            </button>
          </h5>
        </div>
        <div id="collapse-saturn-4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
          <div class="card-body text-justify">
            Saturnteam te ofrece los mejores planes y costos, pensados para ofrecerle a nuestros clientes el servicio que mejor se adapta a sus necesidades. Para conocer el plan que se adapta a tus requerimientos contáctanos a través de nuestro correo info@Saturnteam.com.
          </div>
        </div>
      </div>
      <!-- 5 -->
      <div class="card">
        <div class="card-header card-saturn" id="headingThree">
          <h5 class="mb-0">
            <button class="text-uppercase btn btn-link btn-saturn collapsed btn-block" data-toggle="collapse" data-target="#collapse-saturn-5" aria-expanded="false" aria-controls="collapse-saturn-5">
                5.- ¿Cuáles son los archivos finales a recibr o como recibo mis productos finales?
            </button>
          </h5>
        </div>
        <div id="collapse-saturn-5" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
          <div class="card-body text-justify">
            Los productos que ofrecemos se entregan de manera digital y en los formatos adecuados para su uso y aplicación, brindándole al cliente la disponibilidad ilimitada del recurso adquirido.
          </div>
        </div>
      </div>
      <!-- 6 -->
      <div class="card">
        <div class="card-header card-saturn" id="headingThree">
          <h5 class="mb-0">
            <button class="text-uppercase btn btn-link btn-saturn collapsed btn-block" data-toggle="collapse" data-target="#collapse-saturn-6" aria-expanded="false" aria-controls="collapse-saturn-6">
                6.- ¿Durante y luego del servicio tendre asistencia personalizada?
            </button>
          </h5>
        </div>
        <div id="collapse-saturn-6" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
          <div class="card-body text-justify">
            En Saturnteam cuentas con el mejor servicio de customerservise. Te ofrecemos un acompañamiento y apoyo especializado en el proceso y desarrollo de tu proyecto. Estamos a tu disposición ante cualquier duda que se presente.
          </div>
        </div>
      </div>
      <!-- 7 -->
      <div class="card">
        <div class="card-header card-saturn" id="headingThree">
          <h5 class="mb-0">
            <button class="text-uppercase btn btn-link btn-saturn collapsed btn-block" data-toggle="collapse" data-target="#collapse-saturn-7" aria-expanded="false" aria-controls="collapse-saturn-7">
                7. ¿Donde puedo ver sus trabajos?
            </button>
          </h5>
        </div>
        <div id="collapse-saturn-7" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
          <div class="card-body text-justify">
            Puedes ver nuestro portafolio haciendo click en el siguiente enlace: <a href="https://saturnteam.com/portafolio/">https://saturnteam.com/portafolio/</a>
          </div>
        </div>
      </div>
      <!-- 8 -->
      <div class="card">
        <div class="card-header card-saturn" id="headingThree">
          <h5 class="mb-0">
            <button class="text-uppercase btn btn-link btn-saturn collapsed btn-block" data-toggle="collapse" data-target="#collapse-saturn-8" aria-expanded="false" aria-controls="collapse-saturn-8">
                8. ¿Poseen calificaciones o testimonios de clientes anteriores para saber si son fiables o no?
            </button>
          </h5>
        </div>
        <div id="collapse-saturn-8" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
          <div class="card-body text-justify">
            Te invitamos a conocer los más recientes testimonios y calificaciones de nuestros clientes en los portales donde ofrecemos nuestros servicios, a través de los siguientes enlaces:

            <a href="https://www.freelancer.es/u/Morocoimas">https://www.freelancer.es/u/Morocoimas</a>
            <a href="https://www.workana.com/freelancer/saturnteam?ref=user_dropdown">https://www.workana.com/freelancer/saturnteam?ref=user_dropdown</a>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection