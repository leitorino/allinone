@extends('layouts.publicLayout.public_design')
@section('content')
	
<div class="container">
	<div class="row mt-3 mb-3">
		<div class="col-md-4 col-sm-12 mt-3">
			<div class="card">
			  <div class="card-header text-center">
			    <img src="{{ asset('/images/frontend_images/allcat.png') }}" class="img-fluid" style="width: 151px; height: 151px">
			  </div>
			  <div class="card-body">
			    <h2 class="card-title text-center">Todos</h2>
			    <p class="card-text">Categoria que despliega todos los servicios que Saturn Team puede brindarle para su emprendimiento.</p>
			    <center>
			    <a href="{{ url('/todos-productos') }}" class="btn btn-secondary">Visitar</a>
			    </center>
			  </div>
			</div>
		</div>
@foreach($categories as $cat)
	
		<div class="col-md-4 col-sm-12 mt-3">
			<div class="card">
			  <div class="card-header text-center">
			    <img src="{{ asset('images/backend_images/category/big/'.$cat->image) }}" class="img-fluid" style="max-width: 151px; max-height: 151px">
			  </div>
			  <div class="card-body">
			    <h2 class="card-title text-center">{{$cat->name}}</h2>
			    <p class="card-text">{{$cat->description}}</p>
			    <center>
			    <a href="{{ url('/categoria/'.$cat->url) }}" class="btn btn-secondary">Visitar</a>
			    </center>
			  </div>
			</div>
		</div>

@endforeach
	</div>
</div>


@endsection